#!/bin/bash   

echo "npm install..." 
npm install

echo "bower install"
bower install

echo "grunt build"
grunt build

echo "packing with current timestamp..."
zip -r  $(date +%Y%m%d_%H%M).zip dist/