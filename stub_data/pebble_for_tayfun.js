var UI       = require('ui');
var ajax     = require('ajax');
var vibe     = require('ui/vibe');

var anz      = 0;               // Wie viele Pakete werden empfangen
var machines = [];              // MaschinenKonfiguration
var config   = false;           // config vorhanden?
// -------------------------------------------------------------------------
var main     = new UI.Card({ title     : 'Plant VWS',
                             icon      : 'images/sw_digit_4.png',
                             subtitle  : 'shop floor meets pebble',
                             body      : 'Waiting..'
                           }
                          )
    ;
// -------------------------------------------------\
var ws = new WebSocket('ws://192.168.178.39:8002');//new WebSocket('ws://10.174.112.172:8001')|
// -------------------------------------------------/
main.show();
// --------------------------------------------------

// ---------------------------------------------------ONERROR
ws.onerror = function( error ){
  console.log('WEBSOCKET failed ',error);
};

// --------------------------------------------------ONMESSAGE
ws.onmessage = function ( event ) {

  var jd = JSON.parse( event.data );
  var m  = Array.prototype.filter.call(machines, function (e) { return (e._id === jd.id);} )[0];
  anz++;
  main.title( m._source.linie + '\n'+ m._source.arbeitsfolge );

  switch (jd.type) {

      case 'up_teile'   :                          main.subtitle( (jd.type).slice(3) + ' ' + jd.teile   ); break;
      case 'up_status'  :   vibe.vibrate('double');main.subtitle( (jd.type).slice(3) + ' ' + jd.status  ); break; //STATUS mit vibration
      case 'up_prozess' :                          main.subtitle( (jd.type).slice(3) + ' ' + jd.prozess ); break;

  }
  main.body( (jd.adddat).slice(11) + '\n' + (jd.id).slice(3,9) + '|' + anz );
};

//
// --------------------------------------------------AJAX CONFIG ELASTICSEARCH CALLBACK
function callback( data ){
  machines = data.hits.hits;
  console.log( 'MACHINE LENGTH '+ machines.length //+ ' ' + machines
              //.map( function(e){ return e._source.linie + '/' + e._source.arbeitsfolge;} )
              //.join('\n')
             );
 main.title(    machines.length  );
 main.subtitle( 'Maschinen ' );
}
// --------------------------------------------------AJAX CONFIG ELASTICSEARCH
ajax({url     : 'http://192.168.178.39:9805/rest/mget|_search?size=30',
      type    : 'json',
      async   : true,
      method  : 'GET'
     },
     callback
    );