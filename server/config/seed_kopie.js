/**
 * Populate DB with sample data on server start
 * to disable, edit config/environment/index.js, and set `seedDB: false`
 */

'use strict';

var Thing = require('../api/thing/thing.model');
var User = require('../api/user/user.model');
var Gesuche = require('../api/gesuche/gesuche.model');
var Gesuche_Kategorie = require('../api/gesuche_kategorie/gesuche_kategorie.model');
var Gesuche_Marke = require('../api/gesuche_marke/gesuche_marke.model');
var Gesuche_Region = require('../api/gesuche_region/gesuche_region.model');
var Gesuche_Standorte = require('../api/gesuche_standorte/gesuche_standorte.model');

Thing.find({}).remove(function() {
  Thing.create({
    name : 'Development Tools',
    info : 'Integration with popular tools such as Bower, Grunt, Karma, Mocha, JSHint, Node Inspector, Livereload, Protractor, Jade, Stylus, Sass, CoffeeScript, and Less.'
  }, {
    name : 'Server and Client integration',
    info : 'Built with a powerful and fun stack: MongoDB, Express, AngularJS, and Node.'
  }, {
    name : 'Smart Build System',
    info : 'Build system ignores `spec` files, allowing you to keep tests alongside code. Automatic injection of scripts and styles into your index.html'
  },  {
    name : 'Modular Structure',
    info : 'Best practice client and server structures allow for more code reusability and maximum scalability'
  },  {
    name : 'Optimized Build',
    info : 'Build process packs up your templates as a single JavaScript payload, minifies your scripts/css/images, and rewrites asset names for caching.'
  },{
    name : 'Deployment Ready',
    info : 'Easily deploy your app to Heroku or Openshift with the heroku and openshift subgenerators'
  });
});

User.find({}).remove(function() {
  User.create({
    provider: 'local',
    name: 'Test User',
    email: 'test@test.com',
    password: 'test'
  }, {
    provider: 'local',
    role: 'admin',
    name: 'Admin',
    email: 'admin@admin.com',
    password: 'admin'
  }, function() {
      console.log('finished populating users');
    }
  );
});

Gesuche.find({}).remove(function() {
  Gesuche.create({
    marke: 'Audi',
    standort: 'Ingolstadt',
    region: 'EMD',
    kategorie: 'Flüchtlingshilfe',
    nameDerOrganisation: 'Amnesty International',
    ansprechpartnerIn: 'Frau Dr. Hans Meiser',
    adresse: 'Kurze Straße 5, 12345 Humbug',
    telefon: '12456',
    eMail: 'hans@meiser.de',
    homepage: 'http://www.amnesty-braunschweig.de',
    erreichbar: 'Nachts',
    plzEinsatzort: 'Braunschweig',
    konkreteTaetigkeit: 'Ausgabe von Essen und Decken',
    taetigkeitsbeschreibung: 'Ausgabe von Essen und Decken in der Sporthalle',
    schnuppertagMoeglich: 'nein',
    besondereLeistungen: 'nein',
    zeitaufwandBemerkung: 'mindestens 4 Stunden',
    notwendigeKenntnisse: 'Rudimentäre Englischkentnisse'
  },
  {
    marke: 'Porsche',
    standort: 'Nürnberg',
    region: 'HE',
    kategorie: 'Hilfe',
    nameDerOrganisation: 'Freiwillige Feuerwehr',
    ansprechpartnerIn: 'Herr Volker Hinrichs',
    adresse: 'Hagen 5, 12345 Günzburg',
    telefon: '123456789',
    eMail: '',
    homepage: '',
    erreichbar: '9:00-12:00 Uhr',
    plzEinsatzort: '12345 Osnabrück',
    konkreteTaetigkeit: 'Suppenverteilung',
    taetigkeitsbeschreibung: 'Suppenverteilung am Bahnhof',
    schnuppertagMoeglich: 'Ja',
    besondereLeistungen: 'nein',
    zeitaufwandBemerkung: 'zwischen 6 und 8 Stunden',
    notwendigeKenntnisse: 'keine'
  },
  {
    marke: 'MAN',
    standort: 'Salzgitter',
    region: 'SZ',
    kategorie: 'Sport',
    nameDerOrganisation: '1. FC Salzgitter',
    ansprechpartnerIn: 'Herr Heinz Assauer',
    adresse: 'Am Sportplatz 3, 43221 Salder',
    telefon: '05555-456789',
    eMail: '',
    homepage: '',
    erreichbar: '16:00-18:00 Uhr',
    plzEinsatzort: '12345 Salzgitter',
    konkreteTaetigkeit: 'Trainer für E-Jugend',
    taetigkeitsbeschreibung: 'Trainer für E-Jugned',
    schnuppertagMoeglich: 'Ja',
    besondereLeistungen: 'nein',
    zeitaufwandBemerkung: 'ca. 4 Stunden pro Woche',
    notwendigeKenntnisse: 'Erfahrung im Umgang mit Kinder'
  });
});

Gesuche_Marke.find({}).remove(function() {
  Gesuche_Marke.create({
    marke: 'Volkswagen'
  },
  {
    marke: 'Volkswagen PKW'
  },
  {
    marke: 'Volkswagen Nutzfahrzeuge'
  });
});

Gesuche_Kategorie.find({}).remove(function() {
  Gesuche_Kategorie.create({
    kategorie: 'Kultur'
  },
  {
    kategorie: 'Sport'
  },
  {
    kategorie: 'Organisation'
  },
  {
    kategorie: 'Kirche'
  },
  {
    kategorie: 'Handwerk'
  },
  {
    kategorie: 'Allgmein'
  },
  {
    kategorie: 'Kinder, Schule u. Bildung'
  },
   {
    kategorie: 'Rettungswesen'
   },
  {
    kategorie: 'Soziales'
  }   
  );
});

Gesuche_Region.find({}).remove(function() {
  Gesuche_Region.create({
    region: 'Dresden'
  },
  {
    region: 'OS'
  },
  {
    region: 'SZ'
  },
  {
    region: 'H'
  },
  {
    region: 'KS'
  },
  {
    region: 'WOB'
  },
  {
    region: 'Chemnitz'
  },
  {
    region: 'WF'
  },
  {
    region: 'BS'
  },    
  {
    region: 'PE'
  },
  {
    region: 'EMD'
  },      
  {
    region: 'Zwickau'
  },
  {
    region: 'GF'
  }                      
  );
});

Gesuche_Standorte.find({}).remove(function() {
  Gesuche_Standorte.create({
    standort: 'Dresden'
  },
  {
    standort: 'Hannover'
  },
  {
    standort: 'Osnabrück'
  },
  {
    standort: 'Zwickau'
  },
  {
    standort: 'Wolfsburg'
  }  
  );
});
