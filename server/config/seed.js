/**
 * Populate DB with sample data on server start
 * to disable, edit config/environment/index.js, and set `seedDB: false`
 */

'use strict';

var Article = require('../api/article/article.model');
var User = require('../api/user/user.model');
var Gesuche = require('../api/gesuche/gesuche.model');


User.find({}).remove(function() {
  User.create({
    provider: 'local',
    name: 'Test User',
    email: 'test@test.com',
    password: 'test'
  },
    {
      provider: 'local',
      role: 'admin',
      name: 'Max Mustermann',
      email: 'preview',
      password: 'EgggXyT_E9'
    }, {
    provider: 'local',
    role: 'admin',
    name: 'Admin',
    email: 'admin@admin.com',
    password: 'admin'
  }, function() {
      console.log('finished populating users');
    }
  );
});

