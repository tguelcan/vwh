'use strict';

// Development specific configuration
// ==================================
module.exports = {
  // MongoDB connection options
  mongo: {
    uri: 'mongodb://localhost/test'
    // uri: 'mongodb://abc:abc@ds051953.mongolab.com:51953/manudb01'
  },

  seedDB: true
};
