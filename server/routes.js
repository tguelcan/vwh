/**
 * Main application routes
 */

'use strict';

var errors = require('./components/errors');
var path = require('path');
var express = require('express');

module.exports = function(app) {

  // Insert routes below
  app.use('/api/gesuche_plzs', require('./api/gesuche_plz'));
  app.use('/api/link_kategories', require('./api/link_kategorie'));
  app.use('/api/links', require('./api/link'));
  app.use('/api/motivations', require('./api/motivation'));
  app.use('/api/images', require('./api/image'));
  app.use('/api/article_standorts', require('./api/article_standort'));
  app.use('/api/article_markes', require('./api/article_marke'));
  app.use('/api/article_kategories', require('./api/article_kategorie'));
  app.use('/api/gesuche_standortes', require('./api/gesuche_standorte'));
  app.use('/api/gesuche_regions', require('./api/gesuche_region'));
  app.use('/api/gesuche_markes', require('./api/gesuche_marke'));
  app.use('/api/gesuche_kategories', require('./api/gesuche_kategorie'));
  app.use('/api/gesuches', require('./api/gesuche'));
  app.use('/api/articles', require('./api/article'));
  app.use('/api/users', require('./api/user'));

  app.use('/uploadedImages',express.static(path.join(__dirname, '../uploadedImages')));

  app.use('/auth', require('./auth'));

  // All undefined asset or api routes should return a 404
  app.route('/:url(api|auth|components|app|bower_components|assets|uploadedImages)/*')
   .get(errors[404]);

  // All other routes should redirect to the index.html
  app.route('/*')
    .get(function(req, res) {
      res.sendFile(path.resolve(app.get('appPath') + '/index.html'));
    });
};
