'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var LinkSchema = new Schema({
    title: String,
    bild: String,
    url: String,
    content: String,
    kategorie: {
        type: Schema.Types.ObjectId,
        ref: 'LinkKategorie'
    },
    standort: {
        type: Schema.Types.ObjectId,
        ref: 'ArticleStandort'
    },
    createdBy: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    createdAt: {
        type: Date, default: Date.now
    },
    isToplink: {
        type: Boolean,
        default: false
    }
});

module.exports = mongoose.model('Link', LinkSchema);
