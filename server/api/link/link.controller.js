'use strict';

var _ = require('lodash');
var Link = require('./link.model');

// Get list of links
exports.index = function (req, res) {
  Link.find({})
    .populate({path: 'kategorie'})
    .populate({path: 'standort'})
    .exec(function (err, links) {
      if (err) {
        return handleError(res, err);
      }
      return res.status(200).json(links);
    });
};

// Get a single link
exports.show = function (req, res) {
  Link.findById(req.params.id)
    .populate({path: 'kategorie'})
    .populate({path: 'standort'})
    .exec(function (err, link) {
      if (err) {
        return handleError(res, err);
      }
      if (!link) {
        return res.status(404).send('Not Found');
      }
      return res.json(link);
    });
};

// Get a single link
exports.showPlain = function (req, res) {
  Link.findById(req.params.id)
    .exec(function (err, link) {
      if (err) {
        return handleError(res, err);
      }
      if (!link) {
        return res.status(404).send('Not Found');
      }
      return res.json(link);
    });
};

exports.getFilteredKatStdort = function(req, res) {
  Link.find({'kategorie': req.params.kategorie, 'standort': req.params.standort})
    .exec(function(err, link) {
      if (err) {
        return handleError(res, err);
      }
      if (!link) {
        return res.status(404).send('Not Found');
      }
      return res.json(link);
    });
};

exports.getFilteredKat = function(req, res) {
  Link.find({'kategorie': req.params.kategorie})
    .populate('standort')
    .exec(function(err, link) {
      if (err) {
        return handleError(res, err);
      }
      if (!link) {
        return res.status(404).send('Not Found');
      }
      return res.json(link);
    });
};

exports.getFilteredStdort = function(req, res) {
  Link.find({'standort': req.params.standort})
    .populate('kategorie')
    .exec(function(err, link) {
      if (err) {
        return handleError(res, err);
      }
      if (!link) {
        return res.status(404).send('Not Found');
      }
      return res.json(link);
    });
};

// Creates a new link in the DB.
exports.create = function (req, res) {
  Link.create(req.body, function (err, link) {
    if (err) {
      return handleError(res, err);
    }
    return res.status(201).json(link);
  });
};

// Updates an existing link in the DB.
exports.update = function (req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  Link.findById(req.params.id, function (err, link) {
    if (err) {
      return handleError(res, err);
    }
    if (!link) {
      return res.status(404).send('Not Found');
    }
    var updated = _.merge(link, req.body);
    updated.save(function (err) {
      if (err) {
        return handleError(res, err);
      }
      return res.status(200).json(link);
    });
  });
};

// Deletes a link from the DB.
exports.destroy = function (req, res) {
  Link.findById(req.params.id, function (err, link) {
    if (err) {
      return handleError(res, err);
    }
    if (!link) {
      return res.status(404).send('Not Found');
    }
    link.remove(function (err) {
      if (err) {
        return handleError(res, err);
      }
      return res.status(204).send('No Content');
    });
  });
};

exports.reducedLocationFilter = function (req, res) {

    Link.find({})
        .populate('standort')
        .exec(function (err, results) {
            if (err) { return handleError(res, err);  }

            var standorte = results.map(function (result) {
                return result.standort
            }).reduce(uniqueReduce, []);

            standorte.sort(function(a, b){
                if(a.name < b.name) return -1;
                if(a.name > b.name) return 1;
                return 0;
            });

            return res.status(200).json(standorte);
        });

};

exports.toplinks = function(req, res) {
    Link.find({})
        .populate('standort')
        .populate('kategorie')
        .where('isToplink').equals(true)
        .sort({ createdAt: -1})
        .exec(function(err, results) {
            if (err) { return handleError(res, err); }
            if (!results) {
                return res.status(404).send('Not Found');
            }
            return res.json(results);
        });
};

//Dynamic filter
function uniqueReduce(prev, cur) {
    if (cur && cur.name && prev.indexOf(cur) < 0) {
        return prev.concat([cur]);
    } else {
        return prev;
    }
};

function handleError(res, err) {
  return res.status(500).send(err);
}
