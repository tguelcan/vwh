'use strict';

var express = require('express');
var controller = require('./link.controller');
var auth = require('../../auth/auth.service');

var router = express.Router();

router.get('/locationfilter', controller.reducedLocationFilter);  // Muss ganz oben stehen bleiben
router.get('/toplinks', controller.toplinks);
router.get('/', controller.index);
router.get('/plain/:id', controller.showPlain);
router.get('/kat/:kategorie/stdort/:standort', controller.getFilteredKatStdort);
router.get('/kat/:kategorie', controller.getFilteredKat);
router.get('/stdort/:standort', controller.getFilteredStdort);
router.get('/:id', controller.show);

router.post('/', auth.hasRole('user'), controller.create);
router.put('/:id', auth.hasRole('user'), controller.update);
router.delete('/:id', auth.hasRole('user'), controller.destroy);

module.exports = router;
