'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var mongoosePaginate = require('mongoose-paginate');

var GesucheSchema = new Schema({
  marke: String,
  standort:  {
      type: Schema.Types.ObjectId,
      ref: 'GesucheStandorte'
  },
  region: {
    type: Schema.Types.ObjectId,
    ref: 'GesucheRegion'
  },
  kategorie: {
    type: Schema.Types.ObjectId,
    ref: 'GesucheKategorie'
  },
  nameDerOrganisation: String,
  ansprechpartnerIn: String,
  adresse: String,
  telefon: String,
  eMail: String,
  homepage: String,
  erreichbar: String,
  plzEinsatzort: String,
  konkreteTaetigkeit: String,
  taetigkeitsbeschreibung: String,
  schnuppertagMoeglich: String,
  besondereLeistungen: String,
  zeitaufwandBemerkung: String,
  notwendigeKenntnisse: String
});

GesucheSchema.statics = {
  loadRecent: function(cb) {
    this.find({})
      .populate('kategorie')
      .sort('-date')
      .limit(20)
      .exec(cb);
  }
};


GesucheSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('Gesuche', GesucheSchema);
