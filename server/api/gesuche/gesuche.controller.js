'use strict';

var _ = require('lodash');
var Gesuche = require('./gesuche.model');

// Get a single gesuche
exports.show = function (req, res) {
    Gesuche.findById(req.params.id, function (err, gesuche) {
        if (err) {
            return handleError(res, err);
        }
        if (!gesuche) {
            return res.status(404).send('Not Found');
        }
        return res.json(gesuche);
    });
};

// Get a single gesuche
exports.page = function (req, res) {
    var limit = req.params.limit;
    var page = req.params.page;
    if (!limit || !page) {
        limit = 999999999;
        page = 1;
    }

    Gesuche.paginate({}, {
        limit: limit,
        page: page,
        populate: ['standort', 'marke', 'kategorie']
    }, function (err, gesuches) {
        if (err) {
            return handleError(res, err);
        }
        return res.status(200).json(gesuches);
    });
};

// Liste von Gesuchen nach Kategorie
exports.showKategorie = function (req, res) {
    Gesuche.find({kategorie: req.params.kategorie}, function (err, gesuche) {
        if (err) {
            return handleError(res, err);
        }
        if (!gesuche) {
            return res.status(404).send('Not Found');
        }
        return res.json(gesuche);
    });
};

// Liste nach Region
exports.showRegion = function (req, res) {
    Gesuche.find({region: req.params.region}, function (err, gesuche) {
        if (err) {
            return handleError(res, err);
        }
        if (!gesuche) {
            return res.status(404).send('Not Found');
        }
        return res.json(gesuche);
    });
};

// Liste nach Einsatzort
exports.showEinsatzort = function (req, res) {
    Gesuche.find({plzEinsatzort: req.params.plzeinsatzort}, function (err, gesuche) {
        if (err) {
            return handleError(res, err);
        }
        if (!gesuche) {
            return res.status(404).send('Not Found');
        }
        return res.json(gesuche);
    });
};

// Liste nach Marke
exports.showMarke = function (req, res) {
    Gesuche.find({marke: req.params.marke}, function (err, gesuche) {
        if (err) {
            return handleError(res, err);
        }
        if (!gesuche) {
            return res.status(404).send('Not Found');
        }
        return res.json(gesuche);
    });
};


// Get a single article filtered by location or category
exports.filtered = function (req, res) {
    var page = 1;
    var itemsPerPage = 5;
    var filterQuery = Gesuche
        .find({})
        .populate('standort')
        .populate('region')
        .populate('kategorie');

    if (req.query.hasOwnProperty('location') && req.query.location.length > 0) {
        var locationNames = cleanupArray(req.query.location.split(','));
        filterQuery.where('plzGrouped').in(locationNames);
    }

    if (req.query.hasOwnProperty('category') && req.query.category.length > 0) {
        var categoryNames = cleanupArray(req.query.category.split(','));
        filterQuery.where('kategorie').in(categoryNames);
    }


    if (req.query.hasOwnProperty('page')) {
        page = parseInt(req.query.page) || 1;
    }
    if (req.query.hasOwnProperty('itemsPerPage')) {
        itemsPerPage = parseInt(req.query.itemsPerPage) || 5;
    }
    var skipFrom = (page * itemsPerPage) - itemsPerPage;


    filterQuery
        .sort('+createdAt')
        .exec(function (err, gesuche) {
            if (err) {
                return handleError(res, err);
            }

            var articleTotalCount = gesuche.length,
                resultSet = gesuche.slice(skipFrom, (skipFrom + itemsPerPage));


            return res.status(200).json({
                total: articleTotalCount,
                gesuche: resultSet
            });
        });
};

exports.reducedFilterOptions = function (req, res) {
    var filterQuery = Gesuche
        .find({})
        .populate('standort')
        .populate('region')
        .populate('kategorie')
        .exec(function (err, articles) {
            if (err) { return handleError(res, err); }

            var categories = articles.map(function (gesuche) {
                return gesuche.kategorie
            }).reduce(uniqueReduce, []);
            var standorte = articles.map(function (gesuche) {
                return gesuche.standort
            }).reduce(uniqueReduce, []);


            return res.status(200).json({
                categories: categories,
                locations: standorte
            });
        });

};

function cleanupArray(arrayList) {
    var cleaned = [];

    for (var i = 0, maxI = arrayList.length; i < maxI; i++) {
        var stringToEscape = arrayList[i];
        cleaned.push(stringToEscape.replace(/\$|\./g, ''));
    }

    return cleaned;
};

//Dynamic filter
function uniqueReduce(prev, cur) {
    if (cur && cur._id && prev.indexOf(cur) < 0) {
        return prev.concat([cur]);
    } else {
        return prev;
    }
};


function handleError(res, err) {
    return res.status(500).send(err);
}
