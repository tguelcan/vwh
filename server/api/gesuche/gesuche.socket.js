/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var Gesuche = require('./gesuche.model');

exports.register = function(socket) {
  Gesuche.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  Gesuche.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('gesuche:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('gesuche:remove', doc);
}