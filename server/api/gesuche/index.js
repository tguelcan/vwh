'use strict';

var express = require('express');
var controller = require('./gesuche.controller');
var auth = require('../../auth/auth.service');


var router = express.Router();

router.get('/', controller.page);
router.get('/filtered', controller.filtered);
router.get('/filteroptions', controller.reducedFilterOptions);
router.get('/:id', controller.show);
router.get('/kategorie/:kategorie', controller.showKategorie);
router.get('/marke/:marke', controller.showMarke);
router.get('/region/:region', controller.showRegion);
router.get('/plzeinsatzort/:plzeinsatzort', controller.showEinsatzort);

module.exports = router;
