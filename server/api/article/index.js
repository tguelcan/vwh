'use strict';

var express = require('express');
var controller = require('./article.controller');
var auth = require('../../auth/auth.service');


var router = express.Router();

router.get('/', controller.index);
router.get('/filtered', controller.filtered);
router.get('/filteroptions', controller.reducedFilterOptions);
router.get('/:id', controller.show);
router.get('/edit/:id', controller.oneForEdit);
router.get('/recent/:count/top/:isTop', controller.getRecent);
router.get('/recent/:count/top/:isTop/prefer/:marke', controller.getRecent);
router.post('/', auth.hasRole('user'), controller.create);
router.put('/:id', auth.hasRole('user'), controller.update);
router.delete('/:id', auth.hasRole('user'), controller.destroy);

module.exports = router;
