'use strict';

var _ = require('lodash');
var Article = require('./article.model');

// Get list of articles
exports.index = function (req, res) {
    Article
        .find({})
        .populate('standort')
        .populate('kategorie')
        .populate('marke')
        .populate('createdBy', 'name')
        .populate('modifiedBy', 'name')
        .sort({createdAt: -1})
        .exec(function (err, articles) {
            if (err) {
                return handleError(res, err);
            }
            return res.status(200).json(articles);
        });
};

// Get a single article
exports.show = function (req, res) {
    Article.findById(req.params.id)
        .populate({path: 'standort'})
        .populate({path: 'marke'})
        .populate({path: 'kategorie'})
        .exec(function (err, article) {
            if (err) {
                return handleError(res, err);
            }
            if (!article) {
                return res.status(404).send('Not Found');
            }
            return res.json(article);
        });
};

// Get a single article
exports.oneForEdit = function (req, res) {
    Article.findById(req.params.id)
        .exec(function (err, article) {
            if (err) {
                return handleError(res, err);
            }
            if (!article) {
                return res.status(404).send('Not Found');
            }
            return res.json(article);
        });
};

exports.getRecent = function (req, res) {
    Article.find({"toparticle": req.params.isTop})
        //.limit(req.params.count)
        .sort('-createdAt')
        .populate('marke')
        .exists('image')
        .where('active').equals(true)
        .exec(function (err, articles) {
            if (err) { return handleError(res, err); }
            var resultSet = [];

            // Sortiere Ergebnisse nach Marken und fülle bis zum Limit mit
            // Topartikeln der restlichen Marken auf
            if ( req.params.hasOwnProperty('marke') ) {
                var markenErgebnisse = [],
                    sonstigeErgebnisse = [];

                for (var i=0,maxI=articles.length; i<maxI; i++) {
                    var currentArticle = articles[i];

                    // Wenn der Artikel von der aktuellen Marke ist und noch nicht 2 Artikel heraus gefiltert
                    // wurden, dann diese in markenErgebnisse pushen, sonst in sonstigeErgebnisse
                    if ( currentArticle.marke != null && currentArticle.marke.name == req.params.marke && markenErgebnisse.length >= 2 ) {
                        continue;
                    } else if ( currentArticle.marke != null && currentArticle.marke.name == req.params.marke ) {
                        markenErgebnisse.push(currentArticle);
                    } else {
                        sonstigeErgebnisse.push(currentArticle);
                    }

                    // Wenn die Filterung die maximale Anzahl an Ergebnisse übersteigt,
                    // dann abbrechen
                    if ( (markenErgebnisse.length + sonstigeErgebnisse.length) == req.params.count ) { break; }
                }

                resultSet = markenErgebnisse.concat(sonstigeErgebnisse);
            } else {
                resultSet = articles;
            }

            return res.status(200).json(resultSet.slice(0,req.params.count));
        });
};

// Creates a new article in the DB.
exports.create = function (req, res) {
    Article.create(req.body, function (err, article) {
        if (err) {
            return handleError(res, err);
        }
        return res.status(201).json(article);
    });
};

// Updates an existing article in the DB.
exports.update = function (req, res) {
    if (req.body._id) {
        delete req.body._id;
    }
    Article.findById(req.params.id, function (err, article) {
        if (err) {
            return handleError(res, err);
        }
        if (!article) {
            return res.status(404).send('Not Found');
        }
        var updated = _.extend(article, req.body);
        updated.save(function (err) {
            if (err) {
                return handleError(res, err);
            }
            return res.status(200).json(article);
        });
    });
};

// Deletes a article from the DB.
exports.destroy = function (req, res) {
    Article.findById(req.params.id, function (err, article) {
        if (err) {
            return handleError(res, err);
        }
        if (!article) {
            return res.status(404).send('Not Found');
        }
        article.remove(function (err) {
            if (err) {
                return handleError(res, err);
            }
            return res.status(204).send('No Content');
        });
    });
};


// Get a single article filtered by location or category
exports.filtered = function (req, res) {
    var page = 1;
    var itemsPerPage = 5;
    var filterQuery = Article
        .find({})
        .populate('standort')
        .populate('kategorie')
        .populate('marke')
        .where('active').equals(true);

    if (req.query.hasOwnProperty('location') && req.query.location.length > 0) {
        var locationNames = cleanupArray(req.query.location.split(','));
        filterQuery.where('standort').in(locationNames);
    }

    if (req.query.hasOwnProperty('category') && req.query.category.length > 0 && !req.query.hasOwnProperty('showAll')) {
        var categoryNames = cleanupArray(req.query.category.split(','));
        filterQuery.where('marke').in(categoryNames);
    }

    if (req.query.hasOwnProperty('page')) {
        page = parseInt(req.query.page) || 1;
    }
    if (req.query.hasOwnProperty('itemsPerPage')) {
        itemsPerPage = parseInt(req.query.itemsPerPage) || 5;
    }
    var skipFrom = (page * itemsPerPage) - itemsPerPage;


    filterQuery
        .sort('+createdAt')
        .exec(function (err, articles) {
            if (err) { return handleError(res, err); }
            var articleTotalCount = articles.length,
                resultSet = [];

            // Wenn alle angezeigt werden sollen, dann die mitgegebene Marke bevorzugen
            if ( req.query.hasOwnProperty('category') && req.query.category.length > 0 && req.query.hasOwnProperty('showAll') ) {

                var markenErgebnisse = [],
                    sonstigeErgebnisse = [];
                for (var i=0,maxI=articles.length; i<maxI; i++) {
                    var currentArticle = articles[i];

                    if ( currentArticle.marke != null && currentArticle.marke.name == req.query.category ) {
                        markenErgebnisse.push(currentArticle);
                    } else {
                        sonstigeErgebnisse.push(currentArticle);
                    }
                }

                resultSet = markenErgebnisse.concat(sonstigeErgebnisse);
            } else {
                resultSet = articles;
            }




            return res.status(200).json({
                total: articleTotalCount,
                articles: resultSet.slice(skipFrom, skipFrom + itemsPerPage)
            });
        });
};


exports.reducedFilterOptions = function (req, res) {
    Article
        .find({})
        .populate('standort')
        .populate('kategorie')
        .populate('marke')
        .where('active').equals(true)
        .exec(function (err, articles) {
            if (err) { return handleError(res, err);  }

             var categories = articles.map(function (article) {
                 return article.marke
             }).reduce(uniqueReduce, []);
             var standorte = articles.map(function (article) {
             return article.standort
             }).reduce(uniqueReduce, []);

            standorte.sort(function(a, b){
                if(a.name < b.name) return -1;
                if(a.name > b.name) return 1;
                return 0;
            });

            return res.status(200).json({
                /*total: articleTotalCount,
                articles: resultSet,*/
                 categories: categories,
                 locations: standorte
            });
        });

};


function cleanupArray(arrayList) {
    var cleaned = [];

    for (var i = 0, maxI = arrayList.length; i < maxI; i++) {
        var stringToEscape = arrayList[i];
        cleaned.push(stringToEscape.replace(/\$|\./g, ''));
    }

    return cleaned;
}

//Dynamic filter
function uniqueReduce(prev, cur) {
    if (cur && cur.name && prev.indexOf(cur) < 0) {
        return prev.concat([cur]);
    } else {
        return prev;
    }
};


function handleError(res, err) {
    return res.status(500).send(err);
}
