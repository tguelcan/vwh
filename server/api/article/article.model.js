'use strict';

var mongoose = require('mongoose'),
  Schema = mongoose.Schema;
var mongoosePaginate = require('mongoose-paginate');


var ArticleSchema = new Schema({
  name: String,
  info: String,
  toparticle: String,
  active: {
    type: Boolean,
    default: false
  },
  image: String,
  imagesubtitle: String,
  title: String,
  content: String,
  link: String,
  standort: {
    type: Schema.Types.ObjectId,
    ref: 'ArticleStandort'
  },
  organisation: String,
  tags: Array,
  marke: {
    type: Schema.Types.ObjectId,
    ref: 'ArticleMarke'
  },
  kategorie: {
    type: Schema.Types.ObjectId,
    ref: 'ArticleKategorie'
  },
  createdAt: String,
  createdBy: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  modifiedAt: String,
  modifiedBy: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  videoURL: String
});


ArticleSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('Article', ArticleSchema);
