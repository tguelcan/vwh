'use strict';

var express = require('express');
var controller = require('./gesuche_kategorie.controller');
var auth = require('../../auth/auth.service');


var router = express.Router();

router.get('/', controller.index);
router.get('/:id', controller.show);

module.exports = router;
