'use strict';

var _ = require('lodash');
var GesucheKategorie = require('./gesuche_kategorie.model');

// Get list of gesuche_kategories
exports.index = function(req, res) {
  GesucheKategorie.find(function (err, gesuche_kategories) {
    if(err) { return handleError(res, err); }
    return res.status(200).json(gesuche_kategories);
  });
};

// Get a single gesuche_kategorie
exports.show = function(req, res) {
  GesucheKategorie.findById(req.params.id, function (err, gesuche_kategorie) {
    if(err) { return handleError(res, err); }
    if(!gesuche_kategorie) { return res.status(404).send('Not Found'); }
    return res.json(gesuche_kategorie);
  });
};

function handleError(res, err) {
  return res.status(500).send(err);
}
