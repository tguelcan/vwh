'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var GesucheKategorieSchema = new Schema({
  kategorie: String
});

module.exports = mongoose.model('GesucheKategorie', GesucheKategorieSchema);