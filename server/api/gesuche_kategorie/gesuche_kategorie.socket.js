/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var GesucheKategorie = require('./gesuche_kategorie.model');

exports.register = function(socket) {
  GesucheKategorie.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  GesucheKategorie.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('gesuche_kategorie:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('gesuche_kategorie:remove', doc);
}