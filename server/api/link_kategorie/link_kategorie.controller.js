'use strict';

var _ = require('lodash');
var LinkKategorie = require('./link_kategorie.model');

// Get list of link_kategories
exports.index = function(req, res) {
  LinkKategorie.find(function (err, link_kategories) {
    if(err) { return handleError(res, err); }
    return res.status(200).json(link_kategories);
  });
};

// Get a single link_kategorie
exports.show = function(req, res) {
  LinkKategorie.findById(req.params.id, function (err, link_kategorie) {
    if(err) { return handleError(res, err); }
    if(!link_kategorie) { return res.status(404).send('Not Found'); }
    return res.json(link_kategorie);
  });
};

// Creates a new link_kategorie in the DB.
exports.create = function(req, res) {
  LinkKategorie.create(req.body, function(err, link_kategorie) {
    if(err) { return handleError(res, err); }
    return res.status(201).json(link_kategorie);
  });
};

// Updates an existing link_kategorie in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  LinkKategorie.findById(req.params.id, function (err, link_kategorie) {
    if (err) { return handleError(res, err); }
    if(!link_kategorie) { return res.status(404).send('Not Found'); }
    var updated = _.merge(link_kategorie, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.status(200).json(link_kategorie);
    });
  });
};

// Deletes a link_kategorie from the DB.
exports.destroy = function(req, res) {
  LinkKategorie.findById(req.params.id, function (err, link_kategorie) {
    if(err) { return handleError(res, err); }
    if(!link_kategorie) { return res.status(404).send('Not Found'); }
    link_kategorie.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.status(204).send('No Content');
    });
  });
};

function handleError(res, err) {
  return res.status(500).send(err);
}