/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var LinkKategorie = require('./link_kategorie.model');

exports.register = function(socket) {
  LinkKategorie.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  LinkKategorie.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('link_kategorie:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('link_kategorie:remove', doc);
}