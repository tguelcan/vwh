'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var LinkKategorieSchema = new Schema({
  name: String
});

module.exports = mongoose.model('LinkKategorie', LinkKategorieSchema);
