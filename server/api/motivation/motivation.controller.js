'use strict';

var _ = require('lodash');
var Motivation = require('./motivation.model');

// Get list of motivations
exports.index = function(req, res) {
  Motivation.find(function (err, motivations) {
    if(err) { return handleError(res, err); }
    return res.status(200).json(motivations);
  });
};

// Get a single motivation
exports.show = function(req, res) {
  Motivation.findById(req.params.id, function (err, motivation) {
    if(err) { return handleError(res, err); }
    if(!motivation) { return res.status(404).send('Not Found'); }
    return res.json(motivation);
  });
};

// Creates a new motivation in the DB.
exports.create = function(req, res) {
  Motivation.create(req.body, function(err, motivation) {
    if(err) { return handleError(res, err); }
    return res.status(201).json(motivation);
  });
};

// Updates an existing motivation in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Motivation.findById(req.params.id, function (err, motivation) {
    if (err) { return handleError(res, err); }
    if(!motivation) { return res.status(404).send('Not Found'); }
    var updated = _.merge(motivation, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.status(200).json(motivation);
    });
  });
};

// Deletes a motivation from the DB.
exports.destroy = function(req, res) {
  Motivation.findById(req.params.id, function (err, motivation) {
    if(err) { return handleError(res, err); }
    if(!motivation) { return res.status(404).send('Not Found'); }
    motivation.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.status(204).send('No Content');
    });
  });
};

function handleError(res, err) {
  return res.status(500).send(err);
}