'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var MotivationSchema = new Schema({
  title: String,
  content: String,
  language: String,
  active: {
    type: Boolean, default: true
  },
  createdBy: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  createdAt: {
    type: Date, default: Date.now
  }
});

module.exports = mongoose.model('Motivation', MotivationSchema);
