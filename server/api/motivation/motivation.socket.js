/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var Motivation = require('./motivation.model');

exports.register = function(socket) {
  Motivation.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  Motivation.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('motivation:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('motivation:remove', doc);
}