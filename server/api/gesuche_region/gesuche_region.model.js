'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var GesucheRegionSchema = new Schema({
  region: String
});

module.exports = mongoose.model('GesucheRegion', GesucheRegionSchema);