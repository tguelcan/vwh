'use strict';

var _ = require('lodash');
var GesucheRegion = require('./gesuche_region.model');

// Get list of gesuche_regions
exports.index = function(req, res) {
  GesucheRegion.find(function (err, gesuche_regions) {
    if(err) { return handleError(res, err); }
    return res.status(200).json(gesuche_regions);
  });
};

// Get a single gesuche_region
exports.show = function(req, res) {
  GesucheRegion.findById(req.params.id, function (err, gesuche_region) {
    if(err) { return handleError(res, err); }
    if(!gesuche_region) { return res.status(404).send('Not Found'); }
    return res.json(gesuche_region);
  });
};

function handleError(res, err) {
  return res.status(500).send(err);
}
