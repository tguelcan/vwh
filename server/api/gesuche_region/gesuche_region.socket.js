/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var GesucheRegion = require('./gesuche_region.model');

exports.register = function(socket) {
  GesucheRegion.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  GesucheRegion.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('gesuche_region:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('gesuche_region:remove', doc);
}