'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var GesucheStandorteSchema = new Schema({
  standort: String
});

module.exports = mongoose.model('GesucheStandorte', GesucheStandorteSchema);