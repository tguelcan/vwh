/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var GesucheStandorte = require('./gesuche_standorte.model');

exports.register = function(socket) {
  GesucheStandorte.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  GesucheStandorte.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('gesuche_standorte:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('gesuche_standorte:remove', doc);
}