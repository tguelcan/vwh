'use strict';

var _ = require('lodash');
var GesucheStandorte = require('./gesuche_standorte.model');

// Get list of gesuche_standortes
exports.index = function(req, res) {
  GesucheStandorte.find(function (err, gesuche_standortes) {
    if(err) { return handleError(res, err); }
    return res.status(200).json(gesuche_standortes);
  });
};

// Get a single gesuche_standorte
exports.show = function(req, res) {
  GesucheStandorte.findById(req.params.id, function (err, gesuche_standorte) {
    if(err) { return handleError(res, err); }
    if(!gesuche_standorte) { return res.status(404).send('Not Found'); }
    return res.json(gesuche_standorte);
  });
};

function handleError(res, err) {
  return res.status(500).send(err);
}
