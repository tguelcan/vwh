'use strict';

var _ = require('lodash');
var ArticleKategorie = require('./article_kategorie.model');

// Get list of article_kategories
exports.index = function(req, res) {
  ArticleKategorie.find(function (err, article_kategories) {
    if(err) { return handleError(res, err); }
    return res.status(200).json(article_kategories);
  });
};

// Get a single article_kategorie
exports.show = function(req, res) {
  ArticleKategorie.findById(req.params.id, function (err, article_kategorie) {
    if(err) { return handleError(res, err); }
    if(!article_kategorie) { return res.status(404).send('Not Found'); }
    return res.json(article_kategorie);
  });
};

// Creates a new article_kategorie in the DB.
exports.create = function(req, res) {
  ArticleKategorie.create(req.body, function(err, article_kategorie) {
    if(err) { return handleError(res, err); }
    return res.status(201).json(article_kategorie);
  });
};

// Updates an existing article_kategorie in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  ArticleKategorie.findById(req.params.id, function (err, article_kategorie) {
    if (err) { return handleError(res, err); }
    if(!article_kategorie) { return res.status(404).send('Not Found'); }
    var updated = _.merge(article_kategorie, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.status(200).json(article_kategorie);
    });
  });
};

// Deletes a article_kategorie from the DB.
exports.destroy = function(req, res) {
  ArticleKategorie.findById(req.params.id, function (err, article_kategorie) {
    if(err) { return handleError(res, err); }
    if(!article_kategorie) { return res.status(404).send('Not Found'); }
    article_kategorie.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.status(204).send('No Content');
    });
  });
};

function handleError(res, err) {
  return res.status(500).send(err);
}