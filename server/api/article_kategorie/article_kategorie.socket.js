/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var ArticleKategorie = require('./article_kategorie.model');

exports.register = function(socket) {
  ArticleKategorie.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  ArticleKategorie.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('article_kategorie:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('article_kategorie:remove', doc);
}