'use strict';

var _ = require('lodash');
var Image = require('./image.model');


var uuid = require('node-uuid'),
multiparty = require('multiparty'),
fs = require('fs');

exports.postImage = function (req, res) {
  var tmpDir = "temp/";
  if (!fs.existsSync(tmpDir)){
    fs.mkdirSync(tmpDir);
  }

    var form = new multiparty.Form({uploadDir: tmpDir});
  form.parse(req, function (err, fields, files) {
    var file = files.file[0];
    var contentType = file.headers['content-type'];
    var tmpPath = file.path;
    var extIndex = tmpPath.lastIndexOf('.');
    var extension = (extIndex < 0) ? '' : tmpPath.substr(extIndex);
    // uuid is for generating unique filenames.
    var fileName = uuid.v4() + extension;
    var PATH = "uploadedImages/";
    var destPath = PATH + fileName;

    if (!fs.existsSync(PATH)){
      fs.mkdirSync(PATH);
    }

    // Server side file type checker.
    if (contentType !== 'image/png' && contentType !== 'image/jpeg') {
      console.log("Imageupload failed: contentType not valid - " + contentType);
      fs.unlink(tmpPath);
      return res.status(400).send('Unsupported file type.');
    }

    fs.rename(tmpPath, destPath, function (err) {
      if (err) {
        console.log(err);
        return res.status(400).send('Image is not saved:');
      }
      return res.json(destPath);
    });
  });
};

function handleError(res, err) {
  return res.status(500).send(err);
}
