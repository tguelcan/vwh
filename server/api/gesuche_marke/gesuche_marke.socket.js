/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var GesucheMarke = require('./gesuche_marke.model');

exports.register = function(socket) {
  GesucheMarke.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  GesucheMarke.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('gesuche_marke:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('gesuche_marke:remove', doc);
}