'use strict';

var _ = require('lodash');
var GesucheMarke = require('./gesuche_marke.model');

// Get list of gesuche_markes
exports.index = function(req, res) {
  GesucheMarke.find(function (err, gesuche_markes) {
    if(err) { return handleError(res, err); }

    console.log("abc");
    console.log(gesuche_markes.length);

    return res.status(200).json(gesuche_markes);
  });
};

// Get a single gesuche_marke
exports.show = function(req, res) {
  GesucheMarke.findById(req.params.id, function (err, gesuche_marke) {
    if(err) { return handleError(res, err); }
    if(!gesuche_marke) { return res.status(404).send('Not Found'); }
    return res.json(gesuche_marke);
  });
};

function handleError(res, err) {
  return res.status(500).send(err);
}
