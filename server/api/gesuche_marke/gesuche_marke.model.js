'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var GesucheMarkeSchema = new Schema({
  marke: String
});

module.exports = mongoose.model('GesucheMarke', GesucheMarkeSchema);