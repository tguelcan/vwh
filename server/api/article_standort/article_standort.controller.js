'use strict';

var _ = require('lodash');
var ArticleStandort = require('./article_standort.model');

// Get list of article_standorts
exports.index = function(req, res) {
  ArticleStandort.find(function (err, article_standorts) {
    if(err) { return handleError(res, err); }
    return res.status(200).json(article_standorts);
  });
};

// Get a single article_standort
exports.show = function(req, res) {
  ArticleStandort.findById(req.params.id, function (err, article_standort) {
    if(err) { return handleError(res, err); }
    if(!article_standort) { return res.status(404).send('Not Found'); }
    return res.json(article_standort);
  });
};

// Creates a new article_standort in the DB.
exports.create = function(req, res) {
  ArticleStandort.create(req.body, function(err, article_standort) {
    if(err) { return handleError(res, err); }
    return res.status(201).json(article_standort);
  });
};

// Updates an existing article_standort in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  ArticleStandort.findById(req.params.id, function (err, article_standort) {
    if (err) { return handleError(res, err); }
    if(!article_standort) { return res.status(404).send('Not Found'); }
    var updated = _.merge(article_standort, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.status(200).json(article_standort);
    });
  });
};

// Deletes a article_standort from the DB.
exports.destroy = function(req, res) {
  ArticleStandort.findById(req.params.id, function (err, article_standort) {
    if(err) { return handleError(res, err); }
    if(!article_standort) { return res.status(404).send('Not Found'); }
    article_standort.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.status(204).send('No Content');
    });
  });
};

function handleError(res, err) {
  return res.status(500).send(err);
}