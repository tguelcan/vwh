/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var ArticleStandort = require('./article_standort.model');

exports.register = function(socket) {
  ArticleStandort.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  ArticleStandort.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('article_standort:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('article_standort:remove', doc);
}