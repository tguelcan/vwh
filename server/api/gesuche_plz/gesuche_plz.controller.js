'use strict';

var _ = require('lodash');
var GesuchePlz = require('./gesuche_plz.model');

// Get list of gesuche_plzs
exports.index = function(req, res) {
    GesuchePlz
        .find({})
        //.sort("name", 1)
        .exec(
            function (err, gesuche_plzs) {
                if (err) {
                    return handleError(res, err);
                }
                return res.status(200).json(gesuche_plzs);
            }
        );
};

// Get a single gesuche_plz
exports.show = function(req, res) {
  GesuchePlz.findById(req.params.id, function (err, gesuche_plz) {
    if(err) { return handleError(res, err); }
    if(!gesuche_plz) { return res.status(404).send('Not Found'); }
    return res.json(gesuche_plz);
  });
};

function handleError(res, err) {
  return res.status(500).send(err);
}
