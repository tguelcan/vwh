/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var GesuchePlz = require('./gesuche_plz.model');

exports.register = function(socket) {
  GesuchePlz.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  GesuchePlz.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('gesuche_plz:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('gesuche_plz:remove', doc);
}