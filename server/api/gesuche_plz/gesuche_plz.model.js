'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var GesuchePlzSchema = new Schema({
  name: String
});

module.exports = mongoose.model('GesuchePlz', GesuchePlzSchema);