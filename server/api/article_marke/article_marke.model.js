'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var ArticleMarkeSchema = new Schema({
  name: String,
  info: String,
  active: Boolean
});

module.exports = mongoose.model('ArticleMarke', ArticleMarkeSchema);