'use strict';

var _ = require('lodash');
var ArticleMarke = require('./article_marke.model');

// Get list of article_markes
exports.index = function(req, res) {
  ArticleMarke.find(function (err, article_markes) {
    if(err) { return handleError(res, err); }
    return res.status(200).json(article_markes);
  });
};

// Get a single article_marke
exports.show = function(req, res) {
  ArticleMarke.findById(req.params.id, function (err, article_marke) {
    if(err) { return handleError(res, err); }
    if(!article_marke) { return res.status(404).send('Not Found'); }
    return res.json(article_marke);
  });
};

// Creates a new article_marke in the DB.
exports.create = function(req, res) {
  ArticleMarke.create(req.body, function(err, article_marke) {
    if(err) { return handleError(res, err); }
    return res.status(201).json(article_marke);
  });
};

// Updates an existing article_marke in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  ArticleMarke.findById(req.params.id, function (err, article_marke) {
    if (err) { return handleError(res, err); }
    if(!article_marke) { return res.status(404).send('Not Found'); }
    var updated = _.merge(article_marke, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.status(200).json(article_marke);
    });
  });
};

// Deletes a article_marke from the DB.
exports.destroy = function(req, res) {
  ArticleMarke.findById(req.params.id, function (err, article_marke) {
    if(err) { return handleError(res, err); }
    if(!article_marke) { return res.status(404).send('Not Found'); }
    article_marke.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.status(204).send('No Content');
    });
  });
};

function handleError(res, err) {
  return res.status(500).send(err);
}