/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var ArticleMarke = require('./article_marke.model');

exports.register = function(socket) {
  ArticleMarke.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  ArticleMarke.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('article_marke:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('article_marke:remove', doc);
}