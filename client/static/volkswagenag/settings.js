angular.module('vwhApp')
.constant('SETTINGS',{
        marke: 'Volkswagen Konzern', // Name des Standortes, wie im Filter unter Aktuell angezeigt
        standort: 'Wolfsburg',       // Name des Standortes, wie im Filter unter Aktuell angezeigt
        navigation: {
            alwaysShowLogo: false,
            logoUrl: '/assets/images/gemeinsam-helfen_vwag.png'
        },
        footer: {
            showLogo: true,
            logoUrl: 'assets/images/logo-vwag.jpg',
            //logoAdditionalClasses: 'pull-right',
            copyright: '&copy; Volkswagen AG 2015'
        },
        imprint: {
            title: 'Impressum',
            templateUrl: 'static/volkswagenag/impressum.html',
            //linkTargetUrl: '/impressum2#top'     // "#top" immer am Ende anfügen
        },
        legal: {
            title: 'Datenschutz',
            templateUrl: 'static/volkswagenag/datenschutz.html',
            linkTargetUrl: '/impressum#top'     // "#top" immer am Ende anfügen
        },
        exitPage: {
            disclaimer: 'Wenn Sie auf diesen Link gehen, verlassen Sie die Seiten der Volkswagen AG. Die Volkswagen AG '+
                        'macht sich die durch Links erreichbaren Seiten Dritter nicht zu eigen und ist für deren '+
                        'Inhalte nicht verantwortlich. Es gelten die Nutzungsbedingungen des Dritten.'
        }
    });