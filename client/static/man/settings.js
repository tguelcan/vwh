angular.module('vwhApp')
.constant('SETTINGS',{
        marke: 'MAN',     // Name des Standortes, wie im Filter unter Aktuell angezeigt
        standort: 'München',     // Name des Standortes, wie im Filter unter Aktuell angezeigt
        navigation: {
            alwaysShowLogo: true,
            logoUrl: 'assets/images/man-hilft.png'
        },
        footer: {
            showLogo: true,
            logoUrl: 'assets/images/logo/logo-man.png',
            //logoAdditionalClasses: 'pull-right',
            copyright: '&copy; MAN 2015'
        },
        imprint: {
            title: 'Impressum',
            templateUrl: 'static/man/impressum.html',
            //linkTargetUrl: '/impressum2#top'     // "#top" immer am Ende anfügen
        },
        legal: {
            title: 'Rechtliches',
            templateUrl: 'static/man/datenschutz.html',
            //linkTargetUrl: '/impressum#top'     // "#top" immer am Ende anfügen
        },
        exitPage: {
            disclaimer: 'Wenn Sie auf diesen Link gehen, verlassen Sie die Seiten der MAN SE. Die MAN SE '+
                        'macht sich die durch Links erreichbaren Seiten Dritter nicht zu eigen und ist für deren '+
                        'Inhalte nicht verantwortlich. Es gelten die Nutzungsbedingungen des Dritten.'
        }
    });
