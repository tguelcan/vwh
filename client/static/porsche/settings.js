angular.module('vwhApp')
.constant('SETTINGS',{
        marke: 'Porsche',                   // Name des Standortes, wie im Filter unter Aktuell angezeigt
        standort: 'Stuttgart-Zuffenhausen', // Name des Standortes, wie im Filter unter Aktuell angezeigt
        navigation: {
            alwaysShowLogo: true,
            logoUrl: 'assets/images/porsche-hilft.png'
        },
        footer: {
            showLogo: true,
            logoUrl: 'assets/images/logo/logo-porsche.png',
            //logoAdditionalClasses: 'pull-right',
            copyright: '&copy; 2015 Dr. Ing. h.c. F. Porsche AG.<br/>Alle Rechte vorbehalten'
        },
        imprint: {
            title: 'Impressum',
            templateUrl: 'static/porsche/impressum.html',
            //linkTargetUrl: '/impressum2#top'     // "#top" immer am Ende anfügen
        },
        legal: {
            title: 'Rechtliches',
            templateUrl: 'static/porsche/datenschutz.html',
            //linkTargetUrl: '/impressum#top'     // "#top" immer am Ende anfügen
        },
        exitPage: {
            disclaimer: 'Wenn Sie auf diesen Link gehen, verlassen Sie die Seiten der Dr. Ing. h.c. F. Porsche AG. Die Dr. Ing. h.c. F. Porsche AG '+
                        'macht sich die durch Links erreichbaren Seiten Dritter nicht zu eigen und ist für deren '+
                        'Inhalte nicht verantwortlich. Es gelten die Nutzungsbedingungen des Dritten.'
        }
    });
