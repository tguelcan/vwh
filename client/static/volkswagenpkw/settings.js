angular.module('vwhApp')
.constant('SETTINGS',{
        marke: 'Volkswagen',   // Name der passenden Marke, wie im Filter unter Aktuell angezeigt
        standort: 'Wolfsburg', // Name des Standortes, wie im Filter unter Aktuell angezeigt
        navigation: {
            alwaysShowLogo: true,
            logoUrl: 'assets/images/volkswagen-hilft.png'
        },
        footer: {
            showLogo: true,
            logoUrl: 'assets/images/logo/logo_vw.png',
            logoAdditionalClasses: 'pull-left',
            copyright: '&copy; Volkswagen 2015'
        },
        imprint: {
            title: 'Impressum',
            templateUrl: 'static/volkswagenpkw/impressum.html',
            //linkTargetUrl: '/impressum2#top'     // "#top" immer am Ende anfügen
        },
        legal: {
            title: 'Rechtliches',
            templateUrl: 'static/volkswagenpkw/datenschutz.html',
            //linkTargetUrl: '/impressum#top'     // "#top" immer am Ende anfügen
        },
        exitPage: {
          disclaimer: 'Wenn Sie auf diesen Link gehen, verlassen Sie die Seiten der Volkswagen AG. Die Volkswagen AG '+
          'macht sich die durch Links erreichbaren Seiten Dritter nicht zu eigen und ist für deren '+
          'Inhalte nicht verantwortlich. Es gelten die Nutzungsbedingungen des Dritten.'
        }
    });
