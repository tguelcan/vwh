'use strict';

angular.module('vwhApp')
  .controller('NavbarCtrl', function ($scope, $location, $timeout, $anchorScroll, Auth) {
    $scope.menu = [
     {
        'title': 'Aktionen',
        'link': '/article'
      },
      {
        'title': 'Gesuche',
        'link': '/ehrenamt'
      }

    ];

    $scope.goToAnchor = function(anchor){
      $location.path('/');

      $timeout(function() {
        $anchorScroll.yOffset = 110;
        $anchorScroll(anchor);
        $anchorScroll.yOffset = 0;
      }, 500);
    };

    $scope.isCollapsed = true;
    $scope.isLoggedIn = Auth.isLoggedIn;
    $scope.isAdmin = Auth.isAdmin;
    $scope.getCurrentUser = Auth.getCurrentUser;

    $scope.logout = function() {
      Auth.logout();
      $location.path('/login');
    };

    $scope.isActive = function(route) {
      return route === $location.path();
    };
  });
