angular.module('vwhApp')
    .directive('socialsharing', function() {
        return {
            restrict: 'E',
            //replace: 'true',
            templateUrl: "components/socialsharing/socialsharing.html",
            link: function(scope, elem, attrs) {
                scope.generatedId = Math.floor((1 + Math.random()) * 0x10000) .toString(16).substring(1);
                scope.shareFacebook = attrs.facebook || true;
                scope.shareGoogle   = attrs.google || true;
                scope.shareTwitter  = attrs.twitter || true;
                scope.shareMail     = attrs.mail || true;
                scope.shareTitle    = attrs.title || '';

                scope.facebookActivated = false;
                scope.googleActivated   = false;
                scope.twitterActivated  = false;

                scope.mailDetailUrl  = encodeURIComponent(getURI());
                scope.mailDetailBody = "Hallo,%0D%0A unter folgendem Link findest Du eine interessante Hilfsaktion, die Dich interessieren könnte: %0D%0A"+scope.mailDetailUrl+"%0D%0A %0D%0AMit freundlichen Grüßen ";




                scope.activateSharingFacebook = function() {
                    scope.facebookActivated = true;

                    var boxHeight = 30;
                    var boxWidth = 70;
                    var uri = encodeURIComponent(getURI());
                    //uri = encodeURIComponent('http://vwag-hilft.de/article/56051dce904f153e7cdddea7');
                    /*
                    var srcForIframe = '//www.facebook.com/plugins/like.php?locale=de_DE&href='+ uri
                                       + '&width='+boxWidth+'&height='+boxHeight
                                       + '&layout=button'
                                       + '&action=share'
                                       + '&show_faces=false&share=true'
                                       + '&colorscheme=light';
                    */
                    var srcForIframe = 'http://www.facebook.com/v2.4/plugins/share_button.php?app_id=&channel=&container_width=1170&href='+uri+'&layout=button&locale=de_DE&sdk=joey';


                    $('<iframe />', {
                        src: srcForIframe,
                        scrolling: 'no',
                        frameborder: 0,
                        allowTransparency: 'true',
                        seamless: 'seamless',
                        style: 'width:'+boxWidth+'px;height:'+boxHeight+'px;display:inline-block;'
                    }).appendTo('#socialsharing'+scope.generatedId+ ' .facebook .socialmedia_replacement');

                };


                scope.activateSharingTwitter = function() {
                  scope.twitterActivated = true;

                  var boxHeight = 30;
                  var boxWidth = 70;
                  var uri = encodeURIComponent(getURI());
                  var text = scope.shareTitle;
                  var twittercount = 'none';
                  if (typeof text === 'function') {
                    text = text();
                  }
                  // 120 is the max character count left after twitters automatic url shortening with t.co
                  text = abbreviateText(text, '120');
                  var twitter_enc_uri = encodeURIComponent(uri + text);
                  var twitter_count_url = encodeURIComponent(uri);

                  //uri = encodeURIComponent('http://vwag-hilft.de/article/56051dce904f153e7cdddea7');
                  /*

.                   var twitter_code = '<iframe src="//    '&amp;dnt=true" style="width:' + tw_width + 'px; height:' + tw_height + 'px;"></iframe>';
                   var twitter_code = '<iframe allowtransparency="true" frameborder="0" scrolling="no" src="//platform.twitter.com/widgets/tweet_button.html?url=' + twitter_enc_uri + '&amp;counturl=' + twitter_count_url + '&amp;text=' + text + '&amp;count=' + options.services.twitter.count + '&amp;lang=' + language.services.twitter.language + '&amp;dnt=true" style="width:' + tw_width + 'px; height:' + tw_height + 'px;"></iframe>';
                   var twitter_dummy_btn = '<img src="' + options.services.twitter.dummy_img + '" alt="&quot;Tweet this&quot;-Dummy" class="tweet_this_dummy" />';

                   <a class="twitter-share-button"
                   href="https://twitter.com/intent/tweet?url=https%3A%2F%2Fdev.twitter.com%2Fweb%2Ftweet-button%3Fsrc%3Dtwitter"
                   data-counturl="https://dev.twitter.com/web/tweet-button">
                   Tweet</a>
                   */
                  var srcForIframe = 'http://platform.twitter.com/widgets/tweet_button.html?url='+twitter_enc_uri + '&count=none&counturl=' + twitter_count_url + '&text='+text + '&lang=en'+'&dnt=true';

                  $('<iframe />', {
                    src: srcForIframe,
                    scrolling: 'no',
                    frameborder: 0,
                    allowTransparency: 'true',
                    seamless: 'seamless',
                    style: 'width:'+boxWidth+'px;height:'+boxHeight+'px;display:inline-block;'
                  }).appendTo('#socialsharing'+scope.generatedId+ ' .twitter .socialmedia_replacement');

                };


              scope.activateSharingGoogle = function() {
                scope.googleActivated = true;

                var boxHeight = 30;
                var boxWidth = 130;
                var uri = encodeURIComponent(getURI());

                var gplus_uri = uri;

                // we use the Google+ "asynchronous" code, standard code is flaky if inserted into dom after load
                var gplus_code = '<div class="g-plusone" data-size="medium" data-annotation="none" data-href="' + gplus_uri + '"></div><script type="text/javascript">window.___gcfg = {lang: "de"}; (function() { var po = document.createElement("script"); po.type = "text/javascript"; po.async = true; po.src = "https://apis.google.com/js/platform.js"; var s = document.getElementsByTagName("script")[0]; s.parentNode.insertBefore(po, s); })(); </script>';

                $('#socialsharing'+scope.generatedId+ ' .google .socialmedia_replacement').html(gplus_code);

              };

              // build URI from rel="canonical" or document.location
                function getURI() {
                    var uri = document.location.href;
                    var canonical = $("link[rel=canonical]").attr("href");

                    if (canonical && canonical.length > 0) {
                        if (canonical.indexOf("http") < 0) {
                            canonical = document.location.protocol + "//" + document.location.host + canonical;
                        }
                        uri = canonical;
                    }

                    return uri;
                }
              // abbreviate at last blank before length and add "\u2026" (horizontal ellipsis)
              function abbreviateText(text, length) {
                var abbreviated = decodeURIComponent(text);
                if (abbreviated.length <= length) {
                  return text;
                }

                var lastWhitespaceIndex = abbreviated.substring(0, length - 1).lastIndexOf(' ');
                abbreviated = encodeURIComponent(abbreviated.substring(0, lastWhitespaceIndex)) + "\u2026";

                return abbreviated;
              }

            }
        };
    });
