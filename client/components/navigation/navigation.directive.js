'use strict';

angular.module('vwhApp')
  .directive('navigation', function (routeNavigation) {
    return {
      restrict: "E",
      scope: {
        scrollable: '=fade'
      },
      replace: false,
      templateUrl: "components/navigation/navigation.html",
      controller: function ($scope, $window, screenSize, SETTINGS) {
        $scope.SETTINGS = SETTINGS;
        $scope.routes = routeNavigation.routes;
        $scope.activeRoute = routeNavigation.activeRoute;


        $scope.$watch('scrollable', function() {
        });

        if ($scope.scrollable == true) {

          if (screenSize.is('xs')) {
            // it's a mobile device so fetch a small image
            $scope.boolChangeClass = true;
          }

          //add listener for navbar to resize and scroll
          ("scroll resize".split(" ")).forEach(function(e){
            angular.element($window).bind(e, function () {
              if (this.pageYOffset >= 100) {
                //Navigation Change Class
                $scope.boolChangeClass = true;
              } else if (screenSize.is('xs')) {
                // it's a mobile device so fetch a small image
                $scope.boolChangeClass = true;
              } else {
                //Navigation Change Class
                $scope.boolChangeClass = false;
              }
              $scope.$apply();
            });
          });



        }
        else {
          $scope.boolChangeClass = true;
        }
      }


    };
  });
