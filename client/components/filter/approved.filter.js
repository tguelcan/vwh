angular.module('vwhApp')
    .filter('countapprovedarticles', function () {
        return function (input, mustBeApproved) {
            if ( !typeof input == 'Array')
            if ( typeof mustBeApproved == 'undefined' ) { mustBeApproved = true; }
            var output = [];

            for ( var i= 0,maxI=input.length; i<maxI; i++) {
                if (input[i].active === mustBeApproved) {
                    output.push(input[i])
                }
            }

            return output.length;
        };
    });
