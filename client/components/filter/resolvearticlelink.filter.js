angular.module('vwhApp')
    .filter('resolvearticlelink', function () {
        return function (input, searchFor) {
            if ( typeof input == 'undefined' || typeof searchFor == 'undefined'  ) { return ' '; }
            var output = '';

            for ( var i=0,maxI=input.length; i<maxI; i++ ) {
                if ( input[i]._id == searchFor ) {
                    return input[i].name;
                }
            }

            return output;
        };
    });
