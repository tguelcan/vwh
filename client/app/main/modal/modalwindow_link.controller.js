'use strict';

angular.module('vwhApp')
    .controller('ModalWindowExternalLinkCtrl', function($scope, $modalInstance, $http, linkObject, SETTINGS) {
        $scope.SETTINGS = SETTINGS;

        $scope.close = function() {
            $modalInstance.dismiss('close');
        };
        $scope.goForIt = function() {
            $modalInstance.close(linkObject);
        }
    });