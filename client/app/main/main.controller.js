'use strict';

angular.module('vwhApp')
  .controller('MainCtrl', function ($scope, $http, socket, $location, $rootScope, $cookieStore, $modal, SETTINGS) {
    //Carousel
        $scope.SETTINGS = SETTINGS;
    $scope.myInterval = 10000;
    $scope.noWrapSlides = false;
    $scope.slides = [];

    $scope.goToArticle = function () {
      $location.path('/article');
    };


    $http.get('/api/articles/recent/5/top/true/prefer/'+SETTINGS.marke).success(function (articles) {
      $scope.slides = articles;
    });

    //End Carousel

    //---- MOTIVATION ----

    // Sprachauswahl

    $rootScope.language = "Deutsch";

    $scope.setLanguage = function (language) {
      $rootScope.language = language;
    };

    //End Language


    // GET MOTIVATIONS

    $scope.motivations = [];
    $scope.motivationsTitle = "";

    $http.get('/api/motivations').success(function (motivations) {
      $scope.motivations = motivations;
      socket.syncUpdates('motivation', $scope.motivations);
    });

    // GET ARTICLES

    $scope.articles = [];
    $http.get('/api/articles').success(function (articles) {
      $scope.articles = articles;
      //socket.syncUpdates('article', $scope.articles);
    });

    // get recent articles

    var ARTICLES_PER_SLIDE = 4;
    var RECENTARTICLE_COUNT = 8;
    $scope.recentArticleSlides = [];

    $http.get('/api/articles/recent/' + RECENTARTICLE_COUNT + '/top/false/prefer/'+SETTINGS.marke).success(function (articles) {
      var articleGroups = [];
      // build pre-defined slides for carousel
      for (var i = 0; i <= Math.floor((articles.length - 1) / ARTICLES_PER_SLIDE); i++) {
        articleGroups[i] = articles.slice(i * ARTICLES_PER_SLIDE, (i * ARTICLES_PER_SLIDE) + ARTICLES_PER_SLIDE);
      }
      $scope.recentArticleSlides = articleGroups;
    });


    $scope.$on('$destroy', function () {
      socket.unsyncUpdates('motivation');
      //  socket.unsyncUpdates('article');
    });


    $scope.ehrenamtList = [];
    $scope.cities = [];
    $scope.locationFilter = [];
    $scope.categories = [];
    $scope.categoryFilter = [];
    $scope.paginationTotalCount = 0;
    $scope.paginationCurrentPage = 1;
    $scope.paginationItemsPerPage = 5;
    $scope.paginationMaxSize = 5;

    $scope.plzs = [];
    $scope.plzFilter = '__all';
    $scope.plzFilterLabel = 'Alle';

    /*
     $http.get('/api/gesuche_standortes').success(function (result) {
     $scope.cities = result;
     });

     $http.get('/api/gesuche_kategories').success(function (result) {
     $scope.categories = result;
     });
     */
    $http.get('/api/gesuches/filteroptions').success(function (result) {
      $scope.cities     = result.locations;
      $scope.categories = result.categories;

        $scope.cities.sort(function(a, b){
            if(a.name < b.name) return -1;
            if(a.name > b.name) return 1;
            return 0;
        });
    });

    $http.get('/api/gesuche_plzs').success(function (result) {
      $scope.plzs = result;
    });


    $scope.locationFilterClickHandler = function (location) {
      if (location == '__all') {
        if ($scope.locationFilter.length > 0) {
          $scope.locationFilter = [];
        }

      } else {
        if ($scope.locationFilter.indexOf(location) == -1) {
          $scope.locationFilter.push(location);
        } else {
          $scope.locationFilter.splice($scope.locationFilter.indexOf(location), 1);
        }
      }

      $scope.getFilteredContent();
    };

    $scope.isLocationSelected = function (location) {
      return ( $scope.locationFilter.indexOf(location) > -1);
    };

    // get links

    $scope.links = {};

    $http.get('/api/links').success(function(links) {
      $scope.links = links;
    });


    $scope.categoryFilterClickHandler = function (category) {
      if (category == '__all') {
        if ($scope.categoryFilter.length > 0) {
          $scope.categoryFilter = [];
        }

      } else {
        if ($scope.categoryFilter.indexOf(category) == -1) {
          $scope.categoryFilter.push(category);
        } else {
          $scope.categoryFilter.splice($scope.categoryFilter.indexOf(category), 1);
        }
      }

      $scope.getFilteredContent();
    };

    $scope.isCategorySelected = function (category) {
      return ( $scope.categoryFilter.indexOf(category) > -1);
    };

    $scope.plzFilterClickHandler = function (plz, plzLabel) {
      $scope.plzFilterLabel = plzLabel;
      $scope.plzFilter = plz;

      $scope.getFilteredContent();
    };


    $scope.getFilteredContent = function () {
      var queryParts = [
        'page=' + $scope.paginationCurrentPage,
        'itemsPerPage=' + $scope.paginationItemsPerPage
      ];

      if ($scope.plzFilter.length > 0 && $scope.plzFilter != '__all') {
        queryParts.push('location=' + $scope.plzFilter);
      }

      if ($scope.categoryFilter.length > 0) {
        var concatCategories = $scope.categoryFilter.join(',');
        queryParts.push('category=' + concatCategories);
      }

      $http.get('/api/gesuches/filtered?' + queryParts.join('&')).success(function (result) {
        $scope.paginationTotalCount = result.total;
        $scope.ehrenamtList = result.gesuche;
        //$scope.cities = result.locations;
        //$scope.categories = result.categories;
      });
    };

    $scope.getFilteredContent();



        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // get links

        $scope.links = [];
        $scope.toplinks = [];
        $scope.linksCurrentLocation = '';
        $scope.linksAvailableLocations = [];

        $http.get('/api/links/locationfilter').success(function (result) {
            $scope.linksAvailableLocations = result;
            $scope.linksCurrentLocation = result[0];

            // preselect location
            if ( SETTINGS.hasOwnProperty('standort') && typeof SETTINGS.standort != 'undefined' ) {
                for (var i=0,maxI=result.length; i<maxI; i++) {
                    if ( result[i].name == SETTINGS.standort ) {
                        $scope.linksCurrentLocation = result[i];
                        break;
                    }
                }
            }

            $scope.getLinksForSelectedLocation();
        });

        $http.get('/api/links/toplinks').success(function (result) {
            $scope.toplinks = result;
        });

        $scope.setLinkLocation = function( selectedLocation ) {
            $scope.linksCurrentLocation = selectedLocation;
            $scope.getLinksForSelectedLocation();
        };


        $scope.getLinksForSelectedLocation = function() {
            $http.get('/api/links/stdort/'+ $scope.linksCurrentLocation._id).success(function(result) {
                var resultSet = {};

                for (var i= 0,maxI=result.length; i<maxI; i++) {
                    var key = result[i].kategorie.name;
                    if (typeof(resultSet[key]) == "undefined") resultSet[key] = [];
                    resultSet[key].push(result[i]);
                }

                $scope.links = resultSet;
            });
        };

        function groupBy( array , f ) {
            var groups = {};
            array.forEach( function( o ) {
                var group = JSON.stringify( f(o) );
                groups[group] = groups[group] || [];
                groups[group].push( o );
            });

            return Object.keys(groups).map( function( group )  {
                return groups[group];
            })
        }




    //COOCKIES CHECK AND LOCATION

    $scope.goToEhrenamt = function (ehrenamt) {

      if($cookieStore.get('nutzungsbedingungen')){

        $location.path('ehrenamt_details/' + ehrenamt._id)

      }
      else {

        var modalInstance = $modal.open({
          animation: $scope.animationsEnabled,
          templateUrl: 'components/cookie/cookie.html',
          //controller: 'ModalInstanceCtrl',
          resolve: {
            items: function () {
              return $scope.items;
            }
          }
        });

        modalInstance.result.then(function () {

          $cookieStore.put('nutzungsbedingungen', true);

          $location.path('ehrenamt_details/' + ehrenamt._id)

        }, function () {
          //NOTHING
        });
      }


    };



        $scope.openExternalLinkModal = function(link) {
            var modalInstance = $modal.open({
                animation: false,
                templateUrl: 'modalExternalLink.html',
                controller: 'ModalWindowExternalLinkCtrl',
                resolve: {
                    linkObject: function() { return link; }
                }
            });

            modalInstance.result.then(
                function(link) {
                    window.open( link.url);
                },
                function() {
                }
            );
        };

    });
