'use strict';

angular.module('vwhApp')
  .controller('SettingsCtrl', function ($scope, User, Auth, SETTINGS) {
        $scope.SETTINGS = SETTINGS;
      $scope.errors = {};

      $scope.changePassword = function(form) {
        $scope.submitted = true;
        if(form.$valid) {
          Auth.changePassword( $scope.user.oldPassword, $scope.user.newPassword )
            .then( function() {
              $scope.message = 'Password erfolgreich geändert. :)';
            })
            .catch( function() {
              form.password.$setValidity('mongoose', false);
              $scope.errors.other = 'Ungültiges Passwort :(';
              $scope.message = '';
            });
        }
      };
    });
