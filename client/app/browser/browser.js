'use strict';

angular.module('vwhApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/browser', {
        templateUrl: 'app/browser/browser.html',
        controller: 'BrowserCtrl'
      });
  });
