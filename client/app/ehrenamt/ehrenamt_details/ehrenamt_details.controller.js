'use strict';

angular.module('vwhApp')
  .controller('EhrenamtDetailsCtrl', function ($scope, $routeParams, $http, $location, SETTINGS) {
    $scope.SETTINGS = SETTINGS;

    //CATCH EHRENAMT FROM ROUTE PARAMS AND GET PARAMS
    $http.get('/api/gesuches/' + $routeParams._id).success(function (result) {
      $scope.ehrenamt = result;
      $scope.ehrenamtDetailUrl = window.location.href;
      $scope.ehrenamtDetailSubject = 'Gemeinsam helfen';
      $scope.ehrenamtDetailBody = "Hallo,%0D%0A unter folgendem Link findest Du eine interessante Möglichkeit, Dich ehrenamtlich zu engagieren: %0D%0A"+$scope.ehrenamtDetailUrl+"%0D%0A %0D%0AMit freundlichen Grüßen ";

      //console.log(result);
    });

    $scope.goToEhrenamt = function() {
      $location.path('ehrenamt/');
    }

    $scope.printEhrenamt = function() {
        var selector = '.print_container';
        var navlogo = '.print_container .detailLogo';
        $('body > div').css({display:'none'});
        $(navlogo).html($('.navLogo a').html());
        $(navlogo).css({display:'block'});
        var content = $(selector).clone();
        $('body > div').before(content);
        window.print();
        $(selector).first().remove();
        $(navlogo).css({display:'none'});
        $('body > div').css({display:''});
    }

  });
