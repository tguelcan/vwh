'use strict';

angular.module('vwhApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/ehrenamt_details/:_id', {
        templateUrl: 'app/ehrenamt/ehrenamt_details/ehrenamt_details.html',
        controller: 'EhrenamtDetailsCtrl'
      });
  });
