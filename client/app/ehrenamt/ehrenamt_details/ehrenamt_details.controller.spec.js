'use strict';

describe('Controller: EhrenamtDetailsCtrl', function () {

  // load the controller's module
  beforeEach(module('vwhApp'));

  var EhrenamtDetailsCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    EhrenamtDetailsCtrl = $controller('EhrenamtDetailsCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
