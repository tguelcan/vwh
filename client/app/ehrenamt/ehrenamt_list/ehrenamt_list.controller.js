'use strict';

angular.module('vwhApp')
  .controller('EhrenamtListCtrl', function ($scope, $http, socket, $location, $rootScope, $cookieStore, $modal, SETTINGS) {
    $scope.SETTINGS = SETTINGS;
    $scope.ehrenamtList = [];
    $scope.cities = [];
    $scope.locationFilter = [];
    $scope.categories = [];
    $scope.categoryFilter = [];
    $scope.paginationTotalCount = 0;
    $scope.paginationCurrentPage = 1;
    $scope.paginationItemsPerPage = 10;
    $scope.paginationMaxSize = 5;
    $scope.plzs = [];
    $scope.plzFilter = '__all';
    $scope.plzFilterLabel = 'Alle';


    /*
     $http.get('/api/gesuche_standortes').success(function (result) {
     $scope.cities = result;
     });

     $http.get('/api/gesuche_kategories').success(function (result) {
     $scope.categories = result;
     });
     */
    $http.get('/api/gesuches/filteroptions').success(function (result) {
      $scope.cities = result.locations;
      $scope.categories = result.categories;
    });

    $http.get('/api/gesuche_plzs').success(function (result) {
      $scope.plzs = result;
    });


    $scope.plzFilterClickHandler = function (plz, plzLabel) {
      $scope.plzFilterLabel = plzLabel;
      $scope.plzFilter = plz;

      $scope.getFilteredContent();
    };

    $scope.locationFilterClickHandler = function (location) {
      if (location == '__all') {
        if ($scope.locationFilter.length > 0) {
          $scope.locationFilter = [];
        }

      } else {
        if ($scope.locationFilter.indexOf(location) == -1) {
          $scope.locationFilter.push(location);
        } else {
          $scope.locationFilter.splice($scope.locationFilter.indexOf(location), 1);
        }
      }

      $scope.getFilteredContent();
    };

    $scope.isLocationSelected = function (location) {
      return ( $scope.locationFilter.indexOf(location) > -1);
    };

    $scope.categoryFilterClickHandler = function (category) {
      if (category == '__all') {
        if ($scope.categoryFilter.length > 0) {
          $scope.categoryFilter = [];
        }

      } else {
        if ($scope.categoryFilter.indexOf(category) == -1) {
          $scope.categoryFilter.push(category);
        } else {
          $scope.categoryFilter.splice($scope.categoryFilter.indexOf(category), 1);
        }
      }

      $scope.getFilteredContent();
    };

    $scope.isCategorySelected = function (category) {
      return ( $scope.categoryFilter.indexOf(category) > -1);
    };


    $scope.getFilteredContent = function () {
      var queryParts = [
        'page=' + $scope.paginationCurrentPage,
        'itemsPerPage=' + $scope.paginationItemsPerPage
      ];

      if ($scope.plzFilter.length > 0 && $scope.plzFilter != '__all') {
        queryParts.push('location=' + $scope.plzFilter);
      }

      if ($scope.categoryFilter.length > 0) {
        var concatCategories = $scope.categoryFilter.join(',');
        queryParts.push('category=' + concatCategories);
      }

      $http.get('/api/gesuches/filtered?' + queryParts.join('&')).success(function (result) {
        $scope.paginationTotalCount = result.total;
        $scope.ehrenamtList = result.gesuche;
        //$scope.cities = result.locations;
        //$scope.categories = result.categories;
      });
    };


    $scope.getFilteredContent();


    //COOCKIES CHECK AND LOCATION

    $scope.goToEhrenamt = function (ehrenamt) {

      if($cookieStore.get('nutzungsbedingungen')){

        $location.path('ehrenamt_details/' + ehrenamt._id)

      }
      else {

        var modalInstance = $modal.open({
          animation: $scope.animationsEnabled,
          templateUrl: 'components/cookie/cookie.html',
          //controller: 'ModalInstanceCtrl',
          resolve: {
            items: function () {
              return $scope.items;
            }
          }
        });

        modalInstance.result.then(function () {

          $cookieStore.put('nutzungsbedingungen', true);

          $location.path('ehrenamt_details/' + ehrenamt._id)

        }, function () {
          //NOTHING
        });
      }


    };

    // $scope.goToEhrenamt = function(ehrenamt){
    //   $cookieStore.put('myFavorite','oatmeal');
    //   $location.path('ehrenamt_details/' + ehrenamt._id);
    // }

  });
