'use strict';

angular.module('vwhApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/ehrenamt', {
        name: 'Ehrenamt',
        templateUrl: 'app/ehrenamt/ehrenamt_list/ehrenamt_list.html',
        controller: 'EhrenamtListCtrl'
      });
  });
