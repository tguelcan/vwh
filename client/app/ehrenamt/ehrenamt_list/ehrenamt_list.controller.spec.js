'use strict';

describe('Controller: EhrenamtListCtrl', function () {

  // load the controller's module
  beforeEach(module('vwhApp'));

  var EhrenamtListCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    EhrenamtListCtrl = $controller('EhrenamtListCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
