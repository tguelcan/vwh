'use strict';

describe('Controller: BrowserVwCtrl', function () {

  // load the controller's module
  beforeEach(module('vwhApp'));

  var BrowserVwCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    BrowserVwCtrl = $controller('BrowserVwCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
