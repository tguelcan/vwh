'use strict';

angular.module('vwhApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/browser-vw', {
        templateUrl: 'app/browser-vw/browser-vw.html',
        controller: 'BrowserVwCtrl'
      });
  });
