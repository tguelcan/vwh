'use strict';


angular.module('vwhApp', [
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'ngRoute',
  'btford.socket-io',
  'ui.bootstrap', //Bootstrap Angular AddOn https://angular-ui.github.io/bootstrap/
  'ngWig', //WYSIWYG Editor - https://github.com/stevermeister/ngWig
  'angular.filter', //Angular Filter AddOn - https://github.com/a8m/angular-filter.git
  'ngFileUpload',
  'mwl.confirm', //CONFIRM Function - https://github.com/mattlewis92/angular-bootstrap-confirm#installation
  'duScroll',  //Scroll Functionn - https://github.com/oblador/angular-scroll/
  'frapontillo.bootstrap-switch', //ON OFF Toggle - https://github.com/frapontillo/angular-bootstrap-switch
  'truncate', // angular-truncate
  'ngTagsInput',
  'matchMedia'
])
  .config(function ($routeProvider, $locationProvider, $httpProvider) {
    $routeProvider
      .otherwise({
        redirectTo: '/'
      });

    $locationProvider.html5Mode(true);
    $httpProvider.interceptors.push('authInterceptor');


  })

  .factory('authInterceptor', function ($rootScope, $q, $cookieStore, $location) {
    return {
      // Add authorization token to headers
      request: function (config) {
        config.headers = config.headers || {};
        if ($cookieStore.get('token')) {
          config.headers.Authorization = 'Bearer ' + $cookieStore.get('token');
        }
        return config;
      },

      //Tayfun Gülcan Template

      // Intercept 401s and redirect you to login
      responseError: function(response) {
        if(response.status === 401) {
          $location.path('/login');
          // remove any stale tokens
          $cookieStore.remove('token');
          return $q.reject(response);
        }
        else {
          return $q.reject(response);
        }
      }
    };
  })

  .run(function ($rootScope, $location, Auth) {
    // Redirect to login if route requires auth and you're not logged in
    $rootScope.$on('$routeChangeStart', function (event, next) {
      Auth.isLoggedInAsync(function(loggedIn) {
        if (next.authenticate && !loggedIn) {
          event.preventDefault();
          $location.path('/login');
        }
      });
    });
  });
