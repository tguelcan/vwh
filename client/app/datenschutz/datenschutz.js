'use strict';

angular.module('vwhApp')
    .config(function ($routeProvider) {
        $routeProvider
            .when('/datenschutz', {
                templateUrl: 'app/datenschutz/datenschutz.html',
                controller: 'DatenschutzCtrl'
            });
    });
