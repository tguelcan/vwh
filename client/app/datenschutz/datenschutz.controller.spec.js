'use strict';

describe('Controller: DatenschutzCtrl', function () {

  // load the controller's module
  beforeEach(module('vwhApp'));

  var DatenschutzCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    DatenschutzCtrl = $controller('DatenschutzCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
