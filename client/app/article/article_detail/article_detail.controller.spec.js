'use strict';

describe('Controller: ArticleDetailCtrl', function () {

  // load the controller's module
  beforeEach(module('vwhApp'));

  var ArticleDetailCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ArticleDetailCtrl = $controller('ArticleDetailCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
