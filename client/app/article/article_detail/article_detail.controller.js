'use strict';

angular.module('vwhApp')
  .controller('ArticleDetailCtrl', function ($scope, $http, socket, $routeParams, $location, $sanitize, $timeout, SETTINGS) {
    $scope.SETTINGS = SETTINGS;
    $scope.tags = [];
    $scope.videoURL = "";
    $scope.displayVideoContainer = false;


    function init(){
      $http.get('/api/articles/' + $routeParams.id).success(function(article) {
        $scope.article = article;
        $timeout(function ()
        {
          setMovingImageVideo(article.videoURL);
        },200);

        refreshTags(article);
      });

    }

    function refreshTags(article){
      $scope.tags = [];

      if(article.kategorie) $scope.tags.push(article.kategorie);
      if(article.marke) $scope.tags.push(article.marke);
      if(article.tags.length > 0) article.tags.forEach(function(tag){$scope.tags.push({name: tag})});
    }

    $scope.goToArticle = function() {
      $location.path('article/');
    }

    function setMovingImageVideo(videoid1) {
      $('<iframe />', {
        name: 'movingImage24',
        id:   'movingImage24',
        class: 'movingImage24',
        src: videoid1,
          scrolling: 'no',
          seamless: 'seamless'
      }).appendTo('#videoscript');
    }

    init();

  });
