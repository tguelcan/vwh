'use strict';

angular.module('vwhApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/article/:id', {
        templateUrl: 'app/article/article_detail/article_detail.html',
        controller: 'ArticleDetailCtrl'
      });
  });
