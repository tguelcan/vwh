'use strict';

angular.module('vwhApp')
    .controller('ArticleListCtrl', function ($scope, $http, socket, SETTINGS) {
        $scope.SETTINGS = SETTINGS;
        $scope.articles = [];
        $scope.cities = [];
        $scope.locationFilter = [];
        $scope.categories = [];
        $scope.categoryFilter = [];
        $scope.paginationTotalCount = 0;
        $scope.paginationCurrentPage = 1;
        $scope.paginationItemsPerPage = 10;
        $scope.paginationMaxSize = 10;

        /*
        $http.get('/api/article_standorts').success(function (result) {
            $scope.cities = result;
        });

        $http.get('/api/article_kategories').success(function (result) {
            $scope.categories = result;
        });
        */
        $http.get('/api/articles/filteroptions').success(function (result) {
            $scope.cities     = result.locations;
            $scope.categories = result.categories;

            if ( SETTINGS.hasOwnProperty('standort') && typeof SETTINGS.standort != 'undefined' ) {
                for (var i=0,maxI=result.locations.length; i<maxI; i++) {
                    if ( result.locations[i].name == SETTINGS.standort ) {
                        $scope.locationFilter.push(result.locations[i]._id);
                        break;
                    }
                }
            }

            $scope.getFilteredContent();
        });


        $scope.locationFilterClickHandler = function(location) {
            if ( location == '__all' ) {
                if ( $scope.locationFilter.length > 0 ) {
                    $scope.locationFilter = [];
                }

            } else {
                if ( $scope.locationFilter.indexOf(location) == -1 ) {
                    $scope.locationFilter.push(location);
                } else {
                    $scope.locationFilter.splice( $scope.locationFilter.indexOf(location), 1);
                }
            }

            $scope.getFilteredContent();
        };

        $scope.isLocationSelected = function(location) {
            return ( $scope.locationFilter.indexOf(location) > -1);
        };

        $scope.categoryFilterClickHandler = function(category) {
            if ( category == '__all' ) {
                if ( $scope.categoryFilter.length > 0 ) {
                    $scope.categoryFilter = [];
                }

            } else {
                if ( $scope.categoryFilter.indexOf(category) == -1 ) {
                    $scope.categoryFilter.push(category);
                } else {
                    $scope.categoryFilter.splice( $scope.categoryFilter.indexOf(category), 1);
                }
            }

            $scope.getFilteredContent();
        };

        $scope.isCategorySelected = function(category) {
            return ( $scope.categoryFilter.indexOf(category) > -1);
        };


        $scope.getFilteredContent = function() {
            var queryParts = [
                'page='+ $scope.paginationCurrentPage,
                'itemsPerPage='+ $scope.paginationItemsPerPage
            ];

            if ( $scope.locationFilter.length > 0 ) {
                var concatLocations = $scope.locationFilter.join(',');
                queryParts.push('location='+ concatLocations);
            }

            if ( $scope.categoryFilter.length > 0 ) {
                var concatCategories = $scope.categoryFilter.join(',');
                queryParts.push('category='+ concatCategories);
            } else {
                queryParts.push('category='+ SETTINGS.marke);
                queryParts.push('showAll=true');
            }

            $http.get('/api/articles/filtered?'+ queryParts.join('&')).success(function (result) {
                $scope.paginationTotalCount = result.total;
                $scope.articles = result.articles;
                //$scope.cities = result.locations;
                //$scope.categories = result.categories;
            });
        };


        $scope.getFilteredContent();
    });
