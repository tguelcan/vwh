'use strict';

angular.module('vwhApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/article', {
        name: 'Aktuell',
        templateUrl: 'app/article/article_list/article_list.html',
        controller: 'ArticleListCtrl'
      });
  });
