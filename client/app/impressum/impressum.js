'use strict';

angular.module('vwhApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/impressum', {
        templateUrl: 'app/impressum/impressum.html',
        controller: 'ImpressumCtrl'
      });
  });
