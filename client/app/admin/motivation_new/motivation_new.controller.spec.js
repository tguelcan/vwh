'use strict';

describe('Controller: MotivationNewCtrl', function () {

  // load the controller's module
  beforeEach(module('vwhApp'));

  var MotivationNewCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    MotivationNewCtrl = $controller('MotivationNewCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
