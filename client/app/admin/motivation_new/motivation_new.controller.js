'use strict';

angular.module('vwhApp')
  .controller('MotivationNewCtrl', function ($scope, $http, $location, $routeParams, socket, SETTINGS) {
        $scope.SETTINGS = SETTINGS;

    $scope.motivations = [];

    $scope.title = "";

    $http.get('/api/motivations/' + $routeParams._id).success(function(motivations) {
      $scope.motivations = motivations;
      socket.syncUpdates('motivation', $scope.motivations);
    });

    $scope.newMotivation = function() {
      if($scope.motivations.content === '' || $scope.motivations.language === '') {
        return;
      }
      if($routeParams._id) {
        $http.put('/api/motivations/' + $routeParams._id, {
          language: $scope.motivations.language,
          title: $scope.motivations.title,
          content: $scope.motivations.content
        });
        $scope.motivations = '';
      }
      else {
        $http.post('/api/motivations', {
          title: $scope.motivations.title,
          language: $scope.motivations.language,
          content: $scope.motivations.content
        });
        $scope.motivations = '';
      }
      $location.path('admin/motivation/');
    };

    $scope.cancel = function() {
      $location.path('admin/motivation/');
    };

    $scope.$on('$destroy', function () {
      socket.unsyncUpdates('motivation');
    });


  });
