'use strict';

angular.module('vwhApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/admin/motivation_new', {
        templateUrl: 'app/admin/motivation_new/motivation_new.html',
        controller: 'MotivationNewCtrl',
        authenticate: true
      })
      .when('/admin/motivation_new/:_id', {
        templateUrl: 'app/admin/motivation_new/motivation_new.html',
        controller: 'MotivationNewCtrl',
        authenticate: true
      });
  });
