'use strict';

angular.module('vwhApp')
    .controller('LinksNewCtrl', function ($scope, $http, $location, $routeParams, socket, $modal, Auth, Upload, SETTINGS) {
        $scope.SETTINGS = SETTINGS;

        function init() {
            $scope.editMode = (typeof $routeParams._id !== 'undefined');

            $scope.link = {};
            $scope.standorte = [];
            $scope.updateStandorte();
            $scope.updateKategorien();

            if ($scope.editMode) {
                $http.get('/api/links/plain/' + $routeParams._id)
                    .success(function (link) {
                        $scope.link = link;
                    })
                    .error(function () {
                        alert('Der gesuchte Link konnte nicht geladen werden!');
                    });
            }
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // CRUD functions

        $scope.createLink = function () {
            var linkToSave = $scope.link;
            linkToSave.createdBy = Auth.getCurrentUser()._id;
            linkToSave.createdAt = new Date().toISOString();

            $http.post('/api/links', linkToSave)
                .success(function () {
                    $location.path('/admin/links');
                })
                .error(function () {
                    alert('Speichern des Links schlug leider fehl!');
                });
        };

        $scope.updateLink = function () {
            var linkToUpdate = $scope.link;
            linkToUpdate.modifiedBy = Auth.getCurrentUser()._id;
            linkToUpdate.modifiedAt = new Date().toISOString();

            $http.put('/api/links/' + linkToUpdate._id, linkToUpdate)
                .success(function () {
                    $location.path('/admin/links');
                })
                .error(function () {
                    alert('Der Link konnte nicht gespeichert werden!');
                });
        };

        $scope.deleteLink = function (linkObject) {
            $http.delete('/api/links/' + linkObject._id)
                .success(function () {
                    $location.path('/admin/links');
                })
                .error(function () {
                    alert('Der Link konnte nicht gelöscht werden!');
                });
        };


        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        $scope.saveLink = function () {
            if ($scope.editMode) {
                $scope.updateLink();
            }
            else {
                $scope.createLink();
            }
        };

        $scope.cancel = function () {
            $location.path('admin/links/');
        };

        $scope.updateKategorien = function (selectedItem) {
            $http.get('/api/link_kategories').success(function (kategorien) {
                $scope.kategorien = kategorien;
            });
        };

        $scope.openModalKategorie = function () {
            var modalInstance = $modal.open({
                animation: false,
                templateUrl: 'modalKategorie.html',
                controller: 'ModalWindowKategorieCtrl',
                resolve: {
                    kategorien: function () {
                        return $scope.kategorien;
                    }
                }
            });

            modalInstance.result.then(
                function (selectedKategorien) {
                    $scope.updateKategorien();
                    $scope.link.kategorie = selectedKategorien._id;
                },
                function () {
                    $scope.updateKategorien();
                }
            );
        };

        // we're using articles standorte base data here intentionally
        $scope.updateStandorte = function () {
            $http.get('/api/article_standorts').success(function (standorte) {
                $scope.standorte = standorte;
            });
        };

        $scope.updateKategorien = function () {
            $http.get('/api/link_kategories').success(function (kategorien) {
                $scope.kategorien = kategorien;
            });
        };

        $scope.toplinkClickHandler = function() {
            if ( $scope.link.isToplink ) {
                $scope.link.standort = null;
                $scope.link.kategorie = null;
            }
        };


        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Bild Upload

        $scope.uploadInProgress = true;
        $scope.uploadProgress = 0;

        $scope.clearImage = function(){
            $scope.link.bild = null;
        };

        $scope.onFileSelect = function (image) {
            if (angular.isArray(image)) {
                image = image[0];
            }

            // This is how I handle file types in client side
            if (image.type !== 'image/png' && image.type !== 'image/jpeg') {
                alert('Only PNG and JPEG are accepted.');
                return;
            }

            $scope.uploadInProgress = true;
            $scope.uploadProgress = 0;

            $scope.upload = Upload.upload({
                url: '/api/images',
                method: 'POST',
                file: image
            }).progress(function(event) {
                $scope.uploadProgress = Math.floor(event.loaded / event.total);
                //$scope.$apply();
            }).success(function(data, status, headers, config) {
                $scope.uploadInProgress = false;
                // If you need uploaded file immediately
                $scope.link.bild = data;
            }).error(function(err) {
                $scope.uploadInProgress = false;
                console.log('Error uploading file: ' + err.message || err);
            });
        };

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        init();
    });
