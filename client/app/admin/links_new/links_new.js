'use strict';

angular.module('vwhApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/admin/links_new', {
        templateUrl: 'app/admin/links_new/links_new.html',
        controller: 'LinksNewCtrl'
      })
      .when('/admin/links_new/:_id', {
        templateUrl: 'app/admin/links_new/links_new.html',
        controller: 'LinksNewCtrl',
        authenticate: true
      });
  });
