'use strict';

describe('Controller: LinksNewCtrl', function () {

  // load the controller's module
  beforeEach(module('vwhApp'));

  var LinksNewCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    LinksNewCtrl = $controller('LinksNewCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
