'use strict';

angular.module('vwhApp')
    .controller('LinksCtrl', function ($scope, $http, socket, $routeParams, $location, SETTINGS) {
        $scope.SETTINGS = SETTINGS;
        $scope.tablesorterOrderByField = 'title';
        $scope.tablesorterReverseSort = false;
        $scope.tablesorterChangeField = function( fieldName ) {
            if ( $scope.tablesorterOrderByField == fieldName ) {
                $scope.tablesorterReverseSort = !$scope.tablesorterReverseSort;
            } else {
                $scope.tablesorterOrderByField = fieldName;
                $scope.tablesorterReverseSort  = false;
            }
        };

        //End Language

        function init() {
            $scope.links = [];
            $scope.standorte = [];
            $scope.kategorien = [];
            $scope.selectedStandort = undefined;
            $scope.selectedKategorie = undefined;

            $scope.updateLinks();
        }

        $scope.$on('$destroy', function () {
            socket.unsyncUpdates('link');
        });


        $http.get('/api/links').success(function (links) {
            $scope.links = links;
        });

        $http.get('/api/article_standorts').success(function (standorte) {
            $scope.standorte = standorte;
        });

        $http.get('/api/link_kategories').success(function (kategorien) {
            $scope.kategorien = kategorien;
        });

        $scope.newLink = function () {
            $location.path('admin/links_new/');
        };

        $scope.goToeditLink = function (link) {
            $location.path('admin/links_new/' + link._id);
        };

        $scope.deleteLink = function (link) {
            $http.delete('/api/links/' + link._id);
        };

        $scope.updateLinks = function () {
            var queryUrl = '/api/links';
            if ($scope.selectedKategorie) {
                queryUrl += '/kat/' + $scope.selectedKategorie._id
            }
            if ($scope.selectedStandort) {
                queryUrl += '/stdort/' + $scope.selectedStandort._id
            }
            $http.get(queryUrl).success(function (links) {
                $scope.links = links;
            });
        };

        $scope.selectKategorie = function (kategorie) {
            $scope.selectedKategorie = kategorie;
            $scope.updateLinks();
        };

        $scope.selectStandort = function (standort) {
            $scope.selectedStandort = standort;
            $scope.updateLinks();
        };

        $scope.clearSelectedStandort = function () {
            $scope.selectedStandort = undefined;
            $scope.updateLinks();
        };

        $scope.clearSelectedKategorie = function () {
            $scope.selectedKategorie = undefined;
            $scope.updateLinks();
        };

        $scope.isStandortSelected = function () {
            return $scope.selectedStandort;
        };

        $scope.isKategorieSelected = function () {
            return $scope.selectedKategorie;
        };

        socket.syncUpdates('link', $scope.links);
    });
