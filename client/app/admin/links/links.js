'use strict';

angular.module('vwhApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/admin/links', {
        templateUrl: 'app/admin/links/links.html',
        controller: 'LinksCtrl'
      });
  });
