'use strict';

angular.module('vwhApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/admin/motivation', {
        templateUrl: 'app/admin/motivation/motivation.html',
        controller: 'MotivationCtrl',
        authenticate: true
      });
  });
