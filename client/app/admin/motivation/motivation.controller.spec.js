'use strict';

describe('Controller: MotivationCtrl', function () {

  // load the controller's module
  beforeEach(module('vwhApp'));

  var MotivationCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    MotivationCtrl = $controller('MotivationCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
