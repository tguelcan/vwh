'use strict';

angular.module('vwhApp')
  .controller('MotivationCtrl', function ($scope, $http, socket, $routeParams, $location, $modal, SETTINGS) {
        $scope.SETTINGS = SETTINGS;

    $scope.reallyDelete = function(item) {
      $scope.items = window._.remove($scope.items, function(elem) {
        return elem != item;
      });
    };


    //End Language

    $scope.motivationsList = [];


    $http.get('/api/motivations').success(function(motivations) {
      $scope.motivationsList = motivations;
      socket.syncUpdates('motivation', $scope.motivationsList);
    });

    $scope.newMotivation = function() {
      $location.path('admin/motivation_new/');
    };

    $scope.goToeditMotivation = function(motivation) {
      $location.path('admin/motivation_new/' + motivation._id);
    };

    $scope.deleteMotivation = function(motivation) {
      $http.delete('/api/motivations/' + motivation._id);
    };

    $scope.$on('$destroy', function () {
      socket.unsyncUpdates('motivation');
    });

  });
