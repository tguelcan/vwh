'use strict';

angular.module('vwhApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/admin/article_list', {
        templateUrl: 'app/admin/article/article_list/article_list.html',
        controller: 'AdminArticleListCtrl',
        authenticate: true

      })
      .when('/admin/article_new/:_id', {
      templateUrl: 'app/admin/article/article_new/article_new.html',
      controller: 'ArticleNewCtrl',
        authenticate: true

      });
  });
