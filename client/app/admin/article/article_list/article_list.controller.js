'use strict';

angular.module('vwhApp')
    .controller('AdminArticleListCtrl', function ($scope, $http, $location, socket, $routeParams, SETTINGS) {
        $scope.SETTINGS = SETTINGS;
        $scope.tablesorterOrderByField = 'title';
        $scope.tablesorterReverseSort = false;
        $scope.tablesorterChangeField = function( fieldName ) {
            if ( $scope.tablesorterOrderByField == fieldName ) {
                $scope.tablesorterReverseSort = !$scope.tablesorterReverseSort;
            } else {
                $scope.tablesorterOrderByField = fieldName;
                $scope.tablesorterReverseSort  = false;
            }
        };


        $scope.articles = [];
        $scope.marken = [];

        $http.get('/api/articles').success(function(articles) {
            $scope.articles = articles;
            socket.syncUpdates('article', $scope.articles);
        });
        $scope.$on('$destroy', function () {
            socket.unsyncUpdates('article');
        });


        $scope.newArticle = function() {
            $location.path('/admin/article_new');
        };

        //VORSCHEU BUTTON
        $scope.previewArticle = function(articleObject) {
            $location.path('/article/'+ articleObject._id);
        };

        //EDIT BUTTON
        $scope.editArticle = function(articleObject) {
          $location.path('/admin/article_new/' + articleObject._id);
        };



            //TOGGLE FÜR ACTIVE ODER NICHT ACTIVE BUTTON
            $scope.toggleActiveState = function(articleObject) {
              $http.put('/api/articles/'+ articleObject._id, {
                active: articleObject.active
              });
            };


    });
