'use strict';

describe('Controller: ArticleNewCtrl', function () {

  // load the controller's module
  beforeEach(module('vwhApp'));

  var ArticleNewCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ArticleNewCtrl = $controller('ArticleNewCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
