'use strict';

angular.module('vwhApp')
    .controller('ModalWindowMarkeCtrl', function($scope, $modalInstance, $http, marken) {
        $scope.marken = marken;
        $scope.newMarke = '';

        $scope.save = function() {
            $http.post('/api/article_markes', {name: $scope.newMarke})
                .success(function( result ) {
                    $modalInstance.close(result);
                })
                .error(function() {
                    alert('Marke konnte nicht erzeugt werden!')
                });
        };

        $scope.deleteObject = function(object) {
            $http.delete('/api/article_markes/'+ object._id)
                .then(function() {
                    $http.get('/api/article_markes').success(function(marken) {
                        $scope.marken = marken;
                    });
                });
        };

        $scope.close = function() {
            $modalInstance.dismiss('close');
        }
    });