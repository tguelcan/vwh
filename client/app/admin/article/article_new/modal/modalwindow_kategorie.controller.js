'use strict';

angular.module('vwhApp')
    .controller('ModalWindowKategorieCtrl', function($scope, $modalInstance, $http, kategorien) {
        $scope.kategorien = kategorien;
        $scope.newKategorie = '';

        $scope.save = function() {
            $http.post('/api/article_kategories', {name: $scope.newKategorie})
                .success(function( result ) {
                    $modalInstance.close(result);
                })
                .error(function() {
                    alert('Kategorie konnte nicht erzeugt werden!')
                });
        };

        $scope.deleteObject = function(object) {
            $http.delete('/api/article_kategories/'+ object._id)
                .then(function() {
                    $http.get('/api/article_kategories').success(function(kategorien) {
                        $scope.kategorien = kategorien;
                    });
                });
        };

        $scope.close = function() {
            $modalInstance.dismiss('close');
        }
    });