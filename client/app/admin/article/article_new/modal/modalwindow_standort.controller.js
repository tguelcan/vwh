'use strict';

angular.module('vwhApp')
    .controller('ModalWindowStandortCtrl', function($scope, $modalInstance, $http, standorte) {
        $scope.standorte = standorte;
        $scope.newStandort = '';

        $scope.save = function() {
            $http.post('/api/article_standorts', {name: $scope.newStandort})
                .success(function( result ) {
                    $modalInstance.close(result);
                })
                .error(function() {
                    alert('Standorte konnte nicht erzeugt werden!')
                });
        };

        $scope.deleteObject = function(object) {
            $http.delete('/api/article_standorts/'+ object._id)
                .then(function() {
                    $http.get('/api/article_standorts').success(function(standorte) {
                        $scope.standorte = standorte;
                    });
                });
        };

        $scope.close = function() {
            $modalInstance.dismiss('close');
        }
    });