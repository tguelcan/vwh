'use strict';

angular.module('vwhApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/admin/article_new', {
        templateUrl: 'app/admin/article/article_new/article_new.html',
        controller: 'ArticleNewCtrl',
        authenticate: true
      });
  });
