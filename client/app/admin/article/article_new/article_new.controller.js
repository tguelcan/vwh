'use strict';

angular.module('vwhApp')
  .controller('ArticleNewCtrl', function ($scope, $http, $location, $routeParams, $modal, Auth, Upload, SETTINGS) {
    $scope.editMode = (typeof $routeParams._id != 'undefined');
        $scope.SETTINGS = SETTINGS;
        $scope.standorte = [];
        $scope.marken = [];
        $scope.kategorien = [];

        $scope.tags = [];


        if ($scope.editMode) {
            $http.get('/api/articles/edit/' + $routeParams._id)
                .success(function (article) {
                    $scope.article = article;
                    $scope.tags = convertToTags(article.tags);
                })
                .error(function() {
                    alert('Der gesuchte Artikel konnte nicht geladen werden!');
                });
        } else {
            $scope.article = { toparticle:false };
        }
        $scope.standorteSelected = {}; // Must be an object


    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////

    function extractTags(tags){
      return tags.map(function(tag) { return tag.text; });
    }

    function convertToTags(tags){
      return tags.map(function(tag) { return {"text": tag};});
    }

    $scope.clearImage = function(){
      $scope.article.image = null;
      $scope.article.imagesubtitle = '';
    };

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Neuen Artikel erstellen
    $scope.createArticle = function () {
      var articleToSave = $scope.article;
      articleToSave.createdBy = Auth.getCurrentUser()._id;
      articleToSave.createdAt = new Date().toISOString();
      articleToSave.tags = extractTags($scope.tags);

      $http.post('/api/articles', articleToSave)
        .success(function () {
          $location.path('/admin/article_list');
        })
        .error(function () {
          alert('Speichern des Artikels schlug leider fehl!');
        });
    };

    // Artikel aktualisieren
    $scope.updateArticle = function () {
      var articleToUpdate = $scope.article;
      articleToUpdate.modifiedBy = Auth.getCurrentUser()._id;
      articleToUpdate.modifiedAt = new Date().toISOString();
      articleToUpdate.tags = extractTags($scope.tags);

      $http.put('/api/articles/' + articleToUpdate._id, $scope.article)
        .success(function () {
          $location.path('/admin/article_list');
        })
        .error(function () {
          alert('Der Artikel konnte nicht gespeichert werden!');
        });
    };

    $scope.deleteArticle = function(articleObject) {
        $http.delete('/api/articles/' + articleObject._id)
            .success(function() {
                $location.path('/admin/article_list');
            })
            .error(function() {
                alert('Der Artikel konnte nicht gelöscht werden!');
            });
    };

    $scope.onFileSelect = function (image) {
      if (angular.isArray(image)) {
        image = image[0];
      }

      // This is how I handle file types in client side
      if (image.type !== 'image/png' && image.type !== 'image/jpeg') {
        alert('Only PNG and JPEG are accepted.');
        return;
      }

      $scope.uploadInProgress = true;
      $scope.uploadProgress = 0;

      $scope.upload = Upload.upload({
        url: '/api/images',
        method: 'POST',
        file: image
      }).progress(function(event) {
        $scope.uploadProgress = Math.floor(event.loaded / event.total);
        //$scope.$apply();
      }).success(function(data, status, headers, config) {
        $scope.uploadInProgress = false;
        // If you need uploaded file immediately
        $scope.article.image = data;
      }).error(function(err) {
        $scope.uploadInProgress = false;
        console.log('Error uploading file: ' + err.message || err);
      });
    };

    // Abbrechen
    $scope.abort = function () {
      $location.path('/admin/article_list');
    };

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////

    $scope.updateKategorien = function(selectedItem) {
      $http.get('/api/article_kategories').success(function(kategorien) {
        $scope.kategorien = kategorien;
      });
    };

    $scope.updateMarken = function() {
      $http.get('/api/article_markes').success(function(marken) {
        $scope.marken = marken;
      });
    };

    $scope.updateStandorte = function() {
      $http.get('/api/article_standorts').success(function(standorte) {
        $scope.standorte = standorte;
      });
    };

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////

    $scope.openModalStandort = function() {
      var modalInstance = $modal.open({
        animation: false,
        templateUrl: 'modalStandort.html',
        controller: 'ModalWindowStandortCtrl',
        resolve: {
          standorte: function() { return $scope.standorte; }
        }
      });

      modalInstance.result.then(
        function(selectedStandort) {
          $scope.updateStandorte();
          $scope.article.standort = selectedStandort._id;
        },
        function() {
          $scope.updateStandorte();
        }
      );
    };


    $scope.openModalMarke = function() {
      var modalInstance = $modal.open({
        animation: false,
        templateUrl: 'modalMarke.html',
        controller: 'ModalWindowMarkeCtrl',
        resolve: {
          marken: function() { return $scope.marken; }
        }
      });

      modalInstance.result.then(
        function(selectedMarke) {
          $scope.updateMarken();
          $scope.article.marke = selectedMarke._id;
        },
        function() {
          $scope.updateMarken();
        }
      );
    };


    $scope.openModalKategorie = function() {
      var modalInstance = $modal.open({
        animation: false,
        templateUrl: 'modalKategorie.html',
        controller: 'ModalWindowKategorieCtrl',
        resolve: {
          kategorien: function() { return $scope.kategorien; }
        }
      });

      modalInstance.result.then(
        function(selectedKategorien) {
          $scope.updateKategorien();
          $scope.article.kategorie = selectedKategorien._id;
        },
        function() {
          $scope.updateKategorien();
        }
      );
    };

        $scope.showSaveButton = function(article) {
            if (typeof article == 'undefined') { return false; }

            if ( !article.hasOwnProperty('_id') && typeof $routeParams._id == 'undefined' ) { return true; }
            else { return false; }
        };

        $scope.showFakeSaveButton = function(article) {
            if (typeof article == 'undefined') { return false; }

            if ( article.hasOwnProperty('_id') && typeof $routeParams._id != 'undefined' && (article.hasOwnProperty('active') && article.active == false) ) { return true; }
            else { return false; }
        };

        $scope.showApproveButton = function(article) {
            if (typeof article == 'undefined') { return false; }
            if ( article.hasOwnProperty('_id') && typeof $routeParams._id != 'undefined' && (article.hasOwnProperty('active') && article.active == true) ) {
                return true;
            }

            return false;
        };




        $scope.showSaveButton = function(article) {
            if (typeof article == 'undefined') { return false; }

            if ( !article.hasOwnProperty('_id') && typeof $routeParams._id == 'undefined' ) { return true; }
            else { return false; }
        };

        $scope.showFakeSaveButton = function(article) {
            if (typeof article == 'undefined') { return false; }

            if ( article.hasOwnProperty('_id') && typeof $routeParams._id != 'undefined' && (article.hasOwnProperty('active') && article.active == false) ) { return true; }
            else { return false; }
        };

        $scope.showApproveButton = function(article) {
            if (typeof article == 'undefined') { return false; }
            if ( article.hasOwnProperty('_id') && typeof $routeParams._id != 'undefined' && (article.hasOwnProperty('active') && article.active == true) ) {
                return true;
            }

            return false;
        };




        $scope.updateKategorien();
        $scope.updateMarken();
        $scope.updateStandorte();
    });

// Polyfill für IE <9
if (!Date.prototype.toISOString) {
  (function () {

    function pad(number) {
      if (number < 10) {
        return '0' + number;
      }
      return number;
    }

    Date.prototype.toISOString = function () {
      return this.getUTCFullYear() +
        '-' + pad(this.getUTCMonth() + 1) +
        '-' + pad(this.getUTCDate()) +
        'T' + pad(this.getUTCHours()) +
        ':' + pad(this.getUTCMinutes()) +
        ':' + pad(this.getUTCSeconds()) +
        '.' + (this.getUTCMilliseconds() / 1000).toFixed(3).slice(2, 5) +
        'Z';
    };

  }());
}
